/**
 * CHROME - before window.onload
 * - start scanner if extension is ON
 */
/*
document.onreadystatechange = function(e) {
  if (document.readyState === 'complete') { }
};*/

// If ON - run scanner & sync rules
chrome.storage.local.get(['isOn', 'miniWidget'], function (data) { 
  chrome.runtime.sendMessage({ name: 'get-current-tab-info' }, (tabinfo: tabInfo) => {
    if (data.isOn) { 
          
      // start scanning using common rules (only if not SHOW ALL pressed)
      if (tabinfo.showButtonPressed === false) {
        scanner.start()
      }
    }

    // init miniwidget if ON
    if (data.miniWidget.isOn) {
      if (data.miniWidget.alwaysOn || 
        // if not always on, show miniwidget only if this is the same url on the same tab on page load (reload)
        (tabinfo.foundElemsCount > 0 && tabinfo.url === (new URL(document.location.href)).hostname)
      ) {
        miniwidget.init(data.miniWidget)
      }
    }

    // query local cached or synced rules
    chrome.runtime.sendMessage({
      name: 'sync-rules',
      url: (new URL(document.location.href)).hostname.replace('www.', '')
    }, function (response) {})
  }) 
  
});


/*
chrome.runtime.connect().onDisconnect.addListener(function() {
  // clean up when content script gets disconnected
  scanner.stop()
}) */

/**
 * Listen to messages
 */
chrome.runtime.onMessage.addListener(
  function(request, sender, sendResponse) {
    // console.log('message', { request: request, sender: sender });

    switch (request.name) {

      // from popup.js, on mainbutton ON click
      case 'turn-on':  
        scanner.start()
        miniwidget.initialised ? miniwidget.setStarted() : null
        break

      // from popup.js, on mainbutton OFF click
      case 'turn-off':
        scanner.stop()
        miniwidget.initialised ? miniwidget.setStopped() : null
        break

      // from background.js, on tab switch (if ON only)
      case 'refresh': 
        scanner.isRunning() ? null : scanner.executeScanIteration()
        break
      
      // from popup.js, on 'show all' button activated/deactivated
      case 'show-button-pressed':
        if (request.state === true) {
          scanner.stop()
          miniwidget.setShowFoundElems()
        } else {
          scanner.start()
          miniwidget.setHideFoundElems()
        }
        break

      case 'popup-picker-pressed':
        picker.show()
        break

      // from background.js, on page-specific rule resolved (synced/cached) 
      case 'rules-resolved':
        scanner.pageSpecificRules = request.rules
        scanner.rulesUpdated = true
        //cl('resolved', scanner.pageSpecificRules);

        chrome.storage.local.get('isOn', function (data) { 
          if (data.isOn) { 
            chrome.runtime.sendMessage({ name: 'get-current-tab-info' }, (tabinfo: tabInfo) => {
              if (scanner.isRunning() === false && tabinfo.showButtonPressed === false) {
                scanner.executeScanIteration()
              } 
            })
          }
        })
        break

      // from popup.js, on miniwidget on/off setting change
      case 'mw-settings-onoff':
        if (request.value) {
          chrome.storage.local.get('miniWidget', (mw: MiniWidgetData) => { 
            chrome.runtime.sendMessage({ name: 'get-current-tab-info' }, (tabinfo: tabInfo) => {
              (mw.alwaysOn || tabinfo.foundElemsCount > 0) ? miniwidget.show() : null
            })
          })
        } else {
          miniwidget.hide()
        }
        break
      
      case 'mw-settings-only':
        chrome.storage.local.get('miniWidget', (res) => { 
          chrome.runtime.sendMessage({ name: 'get-current-tab-info' }, (tabinfo: tabInfo) => {
            // always on
            if (request.value) {
              (res.miniWidget.isOn) ? miniwidget.show() : null
            } else {
              (res.miniWidget.isOn && tabinfo.foundElemsCount > 0) ? miniwidget.show() : miniwidget.hide()
            }
          })
        })
        break
        
    }
    
    sendResponse('response from content')
  }
);



