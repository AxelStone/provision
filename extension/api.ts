const API = {

  URL_API: Consts.ENV == 'prod' ? Consts.URL_API_PROD : Consts.URL_API_LOC,

  sendFeedback: (data) => {

    let payload = {
      secret: Consts.SECRET,
      data: data
    }

    fetch(API.URL_API + '/provision/feedback', {
      method: "POST",
      body: JSON.stringify(payload),
      headers: { "Content-type": "application/json; charset=UTF-8" }
    }).then(response => response.json()).then(json => {}
      // console.log(json)
    ).catch(err => console.log(err));
  
  },

  findRules: (url) => {

    let payload = {
      secret: Consts.SECRET,
      url: url
    }

    return fetch(API.URL_API + '/provision/findrules', {
      method: "POST",
      body: JSON.stringify(payload),
      headers: { "Content-type": "application/json; charset=UTF-8" }
    }).then(response => response.json() as Promise<Ruleset[]>);
  },

  login: (email: string, pwd: string) => {

    return fetch(API.URL_API + '/login', {
      method: "POST",
      body: JSON.stringify({ secret: Consts.SECRET, email: email, password: pwd, type: Consts.CLIENT_TYPE }),
      headers: { "Content-type": "application/json; charset=UTF-8" }
    }).then(response => response.json()).then(json => json)
    .catch(err => console.log(err));
  },

  googleLogin: (idtoken: string) => {

    return fetch(API.URL_API + '/googlelogin', {
      method: "POST",
      body: JSON.stringify({ id_token: idtoken, secret: Consts.SECRET, type: Consts.CLIENT_TYPE }),
      headers: { "Content-type": "application/json; charset=UTF-8" }
    }).then(response => response.json()).then(json => json)
    .catch(err => console.log(err));
  },
}