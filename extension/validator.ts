const validator = {

  validateMustContain(uel: unwantedElem, index) {

    let valid = false
    let foundByRuleToValidate = uel.foundByRules[index];
    if (foundByRuleToValidate.mustContain) {
      if (uel.element.querySelector(foundByRuleToValidate.mustContain)) { 
        valid = true;
      }
    }
    return valid; 
  },
  
  validateCookies(uel: unwantedElem, index) {

    let score = 0
    let pos = getComputedStyle(uel.element).position
    let text = uel.element.textContent.toLowerCase()

    if (uel.element.querySelector('iframe')) { score+=4 }
    else if (text.length == 0) { score+=4; }
    else { 

      if (pos == 'absolute' || pos == 'fixed') { score+=2 }

      if (uel.element.className.indexOf('cookie') > -1) { score+=1 }

      if (text.indexOf('cookie') > -1) { score+=2 }
      if (text.indexOf('consent') > -1) { score+=1 }
      if (text.indexOf('agree') > -1) { score+=1 }
      if (text.indexOf('accept') > -1) { score+=1 }
      if (text.indexOf('akcept') > -1) { score+=1 }
      if (text.indexOf('souhlas') > -1) { score+=1 }
      if (text.indexOf('súhlas') > -1) { score+=1 }
      if (text.indexOf('prijať') > -1) { score+=1 }
      if (text.indexOf('přijmout') > -1) { score+=1 }
      if (text.indexOf('soubor') > -1) { score+=1 }
      if (text.indexOf('súbor') > -1) { score+=1 }

      if (uel.element.querySelector('button')) { score+=1 }
      if (uel.element.querySelector('a' + 
        ':not([href*="priva"]):not([href*="poli"]):not([href*="soukrom"]):not([href*="sukrom"])' + 
        ':not([href*="podmink"]:not([href*="podmienk"]'))   { score+=1 }
      if (uel.element.querySelector('a[href*="priva"]'))    { score+=2 } else { if (text.indexOf('priva') > -1)    { score+=1 }}
      if (uel.element.querySelector('a[href*="soukrom"]'))  { score+=2 } else { if (text.indexOf('soukrom') > -1)  { score+=1 }}
      if (uel.element.querySelector('a[href*="sukrom"]'))   { score+=2 } else { if (text.indexOf('sukrom') > -1)   { score+=1 }}
      if (uel.element.querySelector('a[href*="poli"]'))     { score+=2 } else { if (text.indexOf('policy') > -1)   { score+=1 }}
      if (uel.element.querySelector('a[href*="terms"]'))    { score+=2 } else { if (text.indexOf('terms') > -1)    { score+=1 }}
      if (uel.element.querySelector('a[href*="podmink"]'))  { score+=2 } else { if (text.indexOf('podmink') > -1)  { score+=1 }}
      if (uel.element.querySelector('a[href*="podmienk"]')) { score+=2 } else { if (text.indexOf('podmienk') > -1) { score+=1 }}
      
    }

    return score > 3 ? true : false
  },


}