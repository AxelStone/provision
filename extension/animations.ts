const anim = {
  tick: 300,
  mainButtonAnim: null,
  leftEye: document.getElementById('mainbutton').querySelector('svg #lefteye'),
  rightEye: document.getElementById('mainbutton').querySelector('svg #righteye'),

  animWorking: (run) => {

    var didntBlink = 0;

    var working = function() {
      setTimeout(() => anim.setLefteye('on', true), 0);
      setTimeout(() => anim.setLefteye('on', false), anim.tick * 2);
    }

    if (run) {
      anim.mainButtonAnim = setInterval(working, anim.tick * 4);
    } else {
      clearInterval(anim.mainButtonAnim);
    } 
  },

  animTurnOn: () => {
    setTimeout(() => anim.setLefteye('down', true), anim.tick/2);
    setTimeout(() => anim.setLefteye('down', false), anim.tick);
    setTimeout(() => anim.setLefteye('down', true), anim.tick + (anim.tick/2));
    setTimeout(() => anim.setLefteye('down', false), anim.tick * 2);
    setTimeout(() => anim.setRighteye('down', true), anim.tick/2);
    setTimeout(() => anim.setRighteye('down', false), anim.tick);
    setTimeout(() => anim.setRighteye('down', true), anim.tick + (anim.tick/2));
    setTimeout(() => anim.setRighteye('down', false), anim.tick * 2);
  },

  animTurnOff: () => {
    setTimeout(() => anim.setLefteye('down', true), 0);
    setTimeout(() => anim.setLefteye('down', false), anim.tick/2);
    setTimeout(() => anim.setLefteye('down', true), anim.tick);
    setTimeout(() => anim.setLefteye('down', false), anim.tick + (anim.tick/2));
    setTimeout(() => anim.setRighteye('down', true), 0);
    setTimeout(() => anim.setRighteye('down', false), anim.tick/2);
    setTimeout(() => anim.setRighteye('down', true), anim.tick);
    setTimeout(() => anim.setRighteye('down', false), anim.tick + (anim.tick/2));
  },

  setLefteye: (cls, bool) => {
    bool ? anim.leftEye.classList.add(cls) : anim.leftEye.classList.remove(cls);
  },

  setRighteye: (cls, bool) => {
    bool ? anim.rightEye.classList.add(cls) : anim.rightEye.classList.remove(cls);
  }
}
