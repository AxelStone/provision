/**
 * @TODO acceptOnly param
 */


const scanner = {
  // how many times to repeat scan on start and observer's callback
  maxRepeat: 3,
  // current repetition of scan
  iteration: 0,
  // setInterval process
  process: null,
  // Observer objekt
  observer: null,
  // "show all" button state
  showAllPressed: false,
  // cached or synced page-specific rules
  pageSpecificRules: [] as Ruleset[],
  // whether page specific rules has been added (after sync or from cache)
  rulesUpdated: false,
  // cache for all found suspicious elements
  unwantedElements: [] as unwantedElem[],
  // modified elements according to rules of applied selectors
  modifiedElements: [] as modifiedElems[],
  // last scan result cache
  lastResult: {
    foundElemsCount: 0, cookies: 0, push: 0, subscribe: 0, adblocker: 0, other: 0
  } as scanCompletedResult,

  /**
   * Create Mutation observer and timeout the (repeated) scan process
   */
  start() {

    // hide already found elements in case unwanted elements were previously revealed by "SHOW ALL" 
    // button before shutting down the extension
    scanner.showAllPressed = false
    scanner.hideFoundElems()

    // create new observer
    scanner.observer = new MutationObserver(function(mutations) {
      if (!scanner.isRunning()) {
        // setTimeout(scanner.initScanIteration, 100);
        scanner.process = setInterval(scanner.initScanIteration, 400)
      }
    });

    scanner.observer.observe(document.documentElement, {
      attributes: true,
      characterData: false,
      childList: true,
      subtree: true,
      attributeOldValue: false,
      characterDataOldValue: false
    });

    // timeout the scan routine if MutationObserver's callback is not triggered 
    // on document load for some reasons
    setTimeout(() => {
      if (!scanner.isRunning()) {
        scanner.process = setInterval(scanner.initScanIteration, 400);
      }    
    }, 0)

  },

  /**
   * Stop running scan process and remove observer
   */
  stop() {
    scanner.showFoundElems()
    scanner.observer.disconnect()
    scanner.observer = null
    clearInterval(scanner.process)
    scanner.process = null
    scanner.iteration = 0
  },

  /**
   * Whether repeated scan process is running
   */
  isRunning() { 
    return (scanner.process !== null)
  },

  /**
   * Initialise next scan iteration or end scan sequence
   */
  initScanIteration() {

    // repeat only X times
    if (scanner.maxRepeat <= scanner.iteration) {
      // run one more time if page specific rules have been updated during last iteration 
      if (scanner.rulesUpdated) {
        scanner.iteration++
        scanner.executeScanIteration()
      // otherwise cancel repeated scan process
      } else {
        clearInterval(scanner.process)
        scanner.process = null
        scanner.iteration = 0
      }
    } else {
      // run single scan
      scanner.iteration++
      scanner.executeScanIteration()
    }
  },

  /**
   * Executes single scan iteration
   */
  executeScanIteration() {
   
    scanner.rulesUpdated = false
    scanner.scanForSelectors()
    scanner.checkNestedElements()
    scanner.validateElements()
    scanner.processElements()
    scanner.checkScrollLock()
    scanner.uncacheInvalidElements()

    let foundEls = scanner.unwantedElements.filter(el => el.processed === true);
    console.log('found', foundEls)
    
    let res = {
      foundElemsCount: foundEls.length,
      cookies: foundEls.filter(el => el.foundByRules.filter(rule => typeof rule.type == 'undefined' || rule.type == 'cookies').length > 0).length,
      push: foundEls.filter(el => el.foundByRules.filter(rule => rule.type == 'push').length > 0).length,
      subscribe: foundEls.filter(el => el.foundByRules.filter(rule => rule.type == 'subscribe').length > 0).length,
      adblocker: foundEls.filter(el => el.foundByRules.filter(rule => rule.type == 'adblocker').length > 0).length,
      other: foundEls.filter(el => el.foundByRules.filter(rule => rule.type == 'other').length > 0).length,
    }
    chrome.runtime.sendMessage({ 
      name: 'scan-completed', 
      result: res
    }, (res) => {})

    miniwidget.onScanCompleted(res)

    // console.log('(proVision) unwanted elements:', scanner.unwantedElements);
    // console.log('(proVision) modified elements:', scanner.modifiedElements);

  },

  /**
   * Find & cache elements according to all common and site-specific selectors
   * If given elements are already cached just merge selectors
   */
  scanForSelectors() { 

    let url = window.location.hostname

    // scan for hardcoded page specific selectors
    WEBSITES.forEach(website => {
      if (url.indexOf(website.name) > -1) {        
        website.selectors.forEach(rule => { scanner.searchBySelector(rule, 'specific') })
      }
    })

    // scan for synced page specific selectors
    if (scanner.pageSpecificRules) {
      // if (this.rulesUpdated) { console.log('pageSpecificRules updated!', scanner.pageSpecificRules); }    
      scanner.pageSpecificRules.forEach(ruleset => {
        ruleset.rules.forEach(r => {
          // rules with "*" selector are not hiding elements, only processing instructions 
          if (r.selector === '*') { scanner.executeRuleInstructions(r) }
          else { scanner.searchBySelector(r, 'specific') }
        })
      })
    }

    // then scan for common selectors
    COMMON_RULES.forEach(rule => { scanner.searchBySelector(rule, 'common') })

    // console.log('unwanted elements', scanner.unwantedElements)
  },

  /**
   * Find & cache elements according to single selector.
   * If given elements are already cached just merge selectors
   */
  searchBySelector(rule: rule, type: 'common' | 'specific') { 

    let allowedElements = type == 'common' ?
      // common
      ':is(div, span, section, article, aside, footer, figure, p, table, dialog):not([data-provision])' :
      // specific
      ':not(body):not(html):not(form):not(a):not(button):not(input):not(textarea):not(select):not(i):not(svg):not(g)' + 
      ':not(path):not(circle):not(polygon):not(h1):not(h2):not(h3):not(h4):not(h5):not(ul):not(ol):not(li):not(img)' +
      ':not(script):not(link):not(meta):not(style)'+ 
      ':not([data-provision])'

    // Query unwanted elements (omit already cached [data-provision] elements)
    let foundDomEls = document.querySelectorAll(
      rule.selector + allowedElements
    );

    // verify & cache found elements
    [...foundDomEls].forEach((el: HTMLElement) => { 

      let foundByRule = {
        ...rule, 
        type: rule.type == undefined ? 'cookies' : rule.type,
        pageSpecific: type === 'specific' ? true : false,
        valid: false
      }
      
      // cache element & add "unwanted" attribute (only if not already present in unwantedElements)
      let existingEl = scanner.unwantedElements.find(u => u.element == el);
      if (existingEl === undefined) {
        el.setAttribute('data-provision', 'unwanted');
        scanner.unwantedElements.push({
          element: el,
          nested: null,
          valid: null,
          processed: null,
          originalDisplay: el.style.display,
          observer: null,
          foundByRules: [foundByRule],
          executedRules: []
        })

      // if already cached, only merge foundByRules 
      } else {
        existingEl.foundByRules.push(foundByRule)
      }

    })
  },

  /**
   * Find & process nested elements
   */
  checkNestedElements() {

    let nested = document.querySelectorAll('[data-provision="unwanted"] [data-provision="unwanted"]');
    // console.log('(proVision) nested elements', nested);
    [...nested].forEach(n => {
      scanner.unwantedElements.forEach(u => {
        if (n == u.element) {
          n.setAttribute('data-provision', 'nested')
          u.nested = true
        }
      })      
    });
  },

  /**
   * Validate found elements (additional checks)
   */
  validateElements() {

    scanner.unwantedElements.forEach((uel) => {

      // validate only element not processed yet, not nested and not valid yet
      if (uel.processed !== true && uel.nested !== true && uel.valid !== true && scanner.isVisible(uel.element)) {

        uel.valid = false;

        // validate every foundByRule
        uel.foundByRules.forEach((foundByRule, ind) => {

          // First validate mustContain attribute.
          // If valid no further contextual analysis is needed
          if (validator.validateMustContain(uel, ind)) { 
            foundByRule.valid = true 
            uel.valid = true
          } 
          else {

            // cookies rules
            if (foundByRule.type === 'cookies') {
              if (foundByRule.pageSpecific === false || (foundByRule.pageSpecific && foundByRule.validate)) {
                if (validator.validateCookies(uel, ind)) { 
                  foundByRule.valid = true 
                  uel.valid = true
                }
              }              
            // other rules are valid without validation
            } else {
              foundByRule.valid = true
              uel.valid = true
            }
          }
        })    

      }
    })
  },

  /**
   * Hide valid unwanted elements and process their instructions if any
   */
  processElements() {

    // process elements, not if "SHOW ALL" is pressed
    if (!scanner.showAllPressed) {

      scanner.unwantedElements.forEach((uel) => {

        // only if element is not processed yet and is valid
        if (uel.valid === true && uel.processed !== true) {

          // cache original display inline style
          uel.originalDisplay = uel.element.style.display          
          // hide element
          uel.element.style.setProperty('display', 'none', 'important')
          // execute rules
          scanner.executeAllRulesInstructions(uel);
          uel.processed = true
          
          // append observer to observe style changes of element
          // (ensure that display:none !important is not removed by external scripts)
          uel.observer = new MutationObserver((muts) => {
            if (!scanner.showAllPressed && muts.find(m => m.attributeName === 'style') !== undefined) {
              uel.element.style.setProperty('display', 'none', 'important')
            }
          })
          uel.observer.observe(uel.element, {attributes: true})
        }

      })
    }
    // console.log('unwanted:', scanner.unwantedElements);
  },

  /**
   * Execute DOM changes according to rule instructions of element's selectors 
   * and cache modified elements
   * - setStyle: [for: '#id', style: 'string']
   */
  executeAllRulesInstructions(unwantedEl) {

    // process selector rules
    unwantedEl.foundByRules.forEach((rule: rule, i: any) => {
      // if rule has not been executed yet 
      if (unwantedEl.executedRules.indexOf(i) == -1) {
        scanner.executeRuleInstructions(rule)
      }        
      // set current rule as executed (do not process rule again in next iteration);
      unwantedEl.executedRules.push(i)
    })
  },

  executeRuleInstructions(rule: rule) {

    // proces setStyle rule of selector
    if (rule.setStyle !== undefined) {

      rule.setStyle.forEach(ss => {
        // find modified element
        let mEl = document.querySelector(ss.for) as HTMLElement;
        if (mEl) {
          let existingMEl = scanner.modifiedElements.find(el => mEl == el.element)
          // cache into modifiedElements if not already cached
          if (existingMEl === undefined) {
            scanner.modifiedElements.push({
              element: mEl,
              originalStyle: mEl.style.cssText,
              rules: [ss],
            })
          // otherwise merge rules
          } else {
            existingMEl.rules.push(ss);
          }
          // and process modified element
          mEl.setAttribute('data-provision', 'modified');
          mEl.style.cssText = mEl.style.cssText + ' ' + ss.style;
        }
      })
    }
  },

  /**
   * Check & unlock scroll if at leats one processed (valid) unwanted element found.
   * Adds html or body tag to modifiedElements if overflowY is hidden
   */
  checkScrollLock() {

    if (scanner.unwantedElements.filter(u => u.processed === true).length > 0) {
      // html, if not cached yet 
      if (scanner.modifiedElements.find(m => m.element == document.documentElement) == undefined) {
        let htmlstyle = getComputedStyle(document.documentElement)
        if (htmlstyle.overflowY == 'hidden') {
          scanner.modifiedElements.push({
            element: document.documentElement,
            originalStyle: document.documentElement.style.cssText,
            rules: ['scroll']
          });
          document.documentElement.style.setProperty('overflow-y', 'auto', 'important')
          document.documentElement.style.setProperty('position', 'relative', 'important')
          document.documentElement.style.setProperty('min-height', '100vh', 'important')
        }
      }

      // body, if not cached yet
      if (scanner.modifiedElements.find(m => m.element == document.body) == undefined) {
        let bodystyle = getComputedStyle(document.body)
        if (bodystyle.overflowY == 'hidden') {
          scanner.modifiedElements.push({
            element: document.body,
            originalStyle: document.body.style.cssText,
            rules: ['scroll']
          });
          document.body.style.setProperty('overflow-y', 'auto', 'important')
          document.body.style.setProperty('position', 'relative', 'important')
          document.body.style.setProperty('min-height', '100vh', 'important')
        }
      }
    }
  },

  uncacheInvalidElements() {

    // uncache nonexisting elements (removed by js)
    scanner.unwantedElements = scanner.unwantedElements.filter(el => document.body.contains(el.element))
    // uncache valid/processed nested elements (in case of hiding the parent element 
    // of another already hidden element from previous scan iteration)
    scanner.unwantedElements = scanner.unwantedElements.filter(el => {
      if (el.valid) {
        let parentEl = scanner.unwantedElements.find(e => e.valid && e !== el && e.element.contains(el.element))
        if (parentEl !== undefined) {
          console.log('child:', el)
          console.log('parent:', parentEl)
          console.log('setting display:', parentEl)
          // reset original style
          el.element.style.display = (el.originalDisplay == 'none' ? 'auto' : el.originalDisplay);
          // and uncache this nested element
          return false
        } else return true
      } else return true
    })
  },

  /**
   * Whether element is visible to user
   */
  isVisible(elem) {
 
    const style = getComputedStyle(elem);
    if (style.display === 'none') return false;
    if (style.visibility !== 'visible') return false;
    if (parseFloat(style.opacity) < 0.1) return false;

    // check element's and children's height and width
    if (typeof elem.getBoundingClientRect === 'function') {
      let dims = elem.getBoundingClientRect();
      if (dims.width > 0 || dims.height > 0) {
        return true; 
      }
      let allchilds = elem.querySelectorAll('*');
      for(let i = 0; i < allchilds.length; i++) {
        if (typeof allchilds[i].getBoundingClientRect === 'function') {
          let dims = allchilds[i].getBoundingClientRect();
          if (dims.width > 0 || dims.height > 0) { 
            return true;
          }
        }
      }
    }
    
    return false;
  },

  /**
   * Reset original style for unwanted and modified elements
   */
  showFoundElems() { 
    
    // console.log('(proVision) showing.. ', scanner.unwantedElements);
    // console.log('(proVision) reseting.. ', scanner.modifiedElements);
    scanner.showAllPressed = true

    // reset processed unwanted elements display
    scanner.unwantedElements.filter(uel => uel.processed === true).forEach(el => {
      el.element.style.display = (el.originalDisplay == 'none' ? 'auto' : el.originalDisplay);
    });

    // reset modified elements style
    scanner.modifiedElements.forEach(el => {
      el.element.style.cssText = el.originalStyle;
    });
  
  },

  /**
   * Rehide all cached elements and reapply rules for modified elements
   * Called when "Show All" button is turned off (and on scan start)
   */
  hideFoundElems() {

    scanner.showAllPressed = false

    // rehide all cached valid elements excluding nested
    scanner.unwantedElements.filter(uel => !uel.nested && uel.valid).forEach((uel) => {
      uel.element.style.setProperty('display', 'none', 'important')
    })

    // reapply rules for modified elements
    scanner.modifiedElements.forEach(mel => {
      mel.rules.forEach(rule => {
        // reapply scroll rule
        if (rule == 'scroll') {
          mel.element.style.setProperty('overflow-y', 'auto', 'important')
          mel.element.style.setProperty('position', 'relative', 'important')
          mel.element.style.setProperty('min-height', '100vh', 'important')
        // reapply setStyle rule
        } else if (rule.style) {          
          mel.element.style.cssText = mel.element.style.cssText + ' ' + rule.style;
        }
      })
    })    
  },

}