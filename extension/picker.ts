const picker = {
  initialised: false,

  init() {
    const cssUrl = chrome.runtime.getURL('css/picker.css')
    const htmlUrl = chrome.runtime.getURL('picker.html')    

    // insert css and html into document
    return fetch(cssUrl).then((response) => response.text()).then((res: String) => {
    
      res = res.replace(/\{fontRegularUrl\}/g, chrome.runtime.getURL('assets/fonts/SourceSansPro-Regular.ttf'))
      res = res.replace(/\{fontSemiboldUrl\}/g, chrome.runtime.getURL('assets/fonts/SourceSansPro-SemiBold.ttf'))
      document.head.insertAdjacentHTML('beforeend', '<style>' + res + '</style>')
      return fetch(htmlUrl).then((response) => response.text())
    
    }).then((res) => {
      res = res.replace(/\{iconEye\}/g, chrome.runtime.getURL('assets/img/eye.svg'))
      document.body.insertAdjacentHTML('beforeend', res)
      return Promise.resolve()
    }).then(() => {

      // listen to click everywhere when picking popup
      document.body.addEventListener('click', ev => {
        if (document.body.getAttribute('data-provision-picker') == 'popup-picker-picking') {
          ev.preventDefault()
          document.body.setAttribute('data-provision-picker', 'popup-picker-picked')
        }
      })

      picker.initialised = true;
    })
  },

  show() {
    if (!picker.initialised) {
      picker.init()
    } 
    document.body.setAttribute('data-provision-picker', 'popup-picker-picking')
  }
}