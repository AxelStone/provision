const Consts = {
  ENV: 'prod',
  //ENV: 'loc',   
  SECRET: '5d61re5g4r6ef165rh1wrf6d5a',
  URL_FB: "https://www.facebook.com/axelproductions86",
  URL_WEB: "https://www.axelproductions86.com",
  URL_EDITOR: "https://www.axelproductions86.com/provision-rule-editor",
  URL_API_PROD: "https://www.axelproductions86.com/api",
  URL_API_LOC:  "http://axelproductions86.loc/api",
  URL_REGISTER: "https://www.axelproductions86.com/provision-rule-editor?page=register",
  URL_RECOVERY: "https://www.axelproductions86.com/provision-rule-editor?page=recovery",
  CLIENT_TYPE: 2, // 1 - editor, 2 - extension
  CACHE_TIME: 1000*60*60*24,
  CACHE_MAX: 300,
  ERROR_CODES: {
    SOMETHING_WENT_WRONG: '0',
    WRONG_USERNAME_PASSOWRD: '1',
    USER_EXISTS: '2',
    ACCOUNT_NOT_ACTIVATED: '3',
    WRONG_PASSOWRD: '4',
    WRONG_EMAIL: '5',
    RULESET_NAME_EXISTS: '6',
    RULESET_INVALID_NAME: '7',
    RULESET_INVALID_JSON: '8',
    RULESET_DOCUMENTATION_NOT_READED: '9',
    ACCESS_DENIED: '10',
    UNSUCCESSFUL_GOOGLE_LOGIN: '10',
  }
}

function cl($val1, $val2 = null) {
  if ($val2 == null) {
    console.log('(proVision) log: ', $val1)
  } else {
    console.log('(proVision) '+$val1+': ', $val2)
  }
}

interface unwantedElem {
  element: HTMLElement,
  nested: null | boolean,
  valid: null | boolean,
  processed: null | boolean,
  originalDisplay: string,
  observer: MutationObserver,
  foundByRules: foundByRule[],
  executedRules: number[]
}

interface modifiedElems {
  element: HTMLElement,
  originalStyle: string,
  rules: (setStyleInstruction | 'scroll')[],
}

interface rule {
  selector: string,
  type?: 'cookies' | 'adblocker' | 'push' | 'subscribe' | 'other',
  mustContain?: string,
  setStyle?: setStyle,
  validate?: boolean 
  descr?: string 
}

interface foundByRule {
  selector: string,
  type: 'cookies' | 'adblocker' | 'push' | 'subscribe' | 'other',
  pageSpecific: boolean,
  mustContain?: string,
  setStyle?: setStyle,
  validate?: boolean 
  descr?: string 
  valid: boolean
}

type setStyle = setStyleInstruction[]

interface setStyleInstruction { for: string, style: string }

interface tabsInfo {
  [key: string]: tabInfo
}

interface tabInfo {
  cookies: number
  push: number
  subscribe: number
  adblocker: number
  other: number
  foundElemsCount: number
  showButtonPressed: boolean
  url: string
}

type Ruleset = { 
  // name of ruleset (e.g "google")
  name: string
  // empty when nothing found on server for this url 
  rules: rule[]
  // other fields are present only when ruleset was found on server
  id?: string
  adult?: string
  author_id?: string
  created?: string
  modified?: string
  modified_by_id?: string
  status?: number
}

type RuleCache = {
  // full url of tab that requested API sync (e.g. "accounts.google.com")
  for: string,
  rulesets: Ruleset[],
  cached: number,
} []


interface scanCompletedResult {
  foundElemsCount: number
  cookies: number
  push: number
  subscribe: number
  adblocker: number
  other: number
}

interface MiniWidgetData {
  isOn: boolean
  alwaysOn: boolean
  positionX: number
}

type LoggedUser = {
  user: {
    created: string
    email: string
    id: string
    nickname: string
    password: string
    roles: string
    status: string
    type: string
    user_id: string | null
  },
  token: string,
  posts: Suggestion[]
}

type Suggestion = {
  id: string,
  ruleset_id: string,
  name: string,
  rules: string,
  author_id: string,
  status: string,
  adult: string,
  created: string,
}

type Err = {
  code: string,
  text: string
}
