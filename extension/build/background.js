let tabsinfo = {};
chrome.runtime.onInstalled.addListener(function () {
    chrome.storage.local.set({
        isOn: true,
        loggedUser: null,
        pageSpecificRuleCache: [],
        miniWidget: {
            isOn: true, alwaysOn: false, positionX: -1
        },
    }, function () {
        console.log("(proVision) proVision Chrome extension: up & running");
        chrome.browserAction.setBadgeBackgroundColor({ color: '#555555' });
    });
});
chrome.runtime.onMessage.addListener(function (message, sender, sendResponse) {
    switch (message.name) {
        case 'sync-rules':
            chrome.storage.local.get(['pageSpecificRuleCache'], function (data) {
                let timestamp = (new Date()).getTime();
                let validLocalCachedItem = data.pageSpecificRuleCache.find(ruleset => {
                    if ((message.url == ruleset.for) && (timestamp - ruleset.cached < Consts.CACHE_TIME))
                        return true;
                    return false;
                });
                if (validLocalCachedItem === undefined) {
                    API.findRules(message.url).then(apiFoundRulesets => {
                        let newRuleCache = getNewRuleCache(data.pageSpecificRuleCache, apiFoundRulesets, message.url, timestamp);
                        chrome.storage.local.set({ pageSpecificRuleCache: newRuleCache }, function () {
                            chrome.tabs.sendMessage(sender.tab.id, { name: 'rules-resolved', rules: apiFoundRulesets });
                        });
                    }).catch(err => console.log(err));
                }
                else {
                    chrome.tabs.sendMessage(sender.tab.id, { name: 'rules-resolved', rules: validLocalCachedItem.rulesets });
                }
            });
            sendResponse(tabsinfo[sender.tab.id]);
            break;
        case 'turn-on':
            tabsinfo[message.tabId === undefined ? sender.tab.id : message.tabId].showButtonPressed = false;
            chrome.browserAction.setIcon({ path: { 16: "assets/img/icon16.png" } });
            sendResponse(tabsinfo[message.tabId]);
            break;
        case 'turn-off':
            tabsinfo[message.tabId === undefined ? sender.tab.id : message.tabId].showButtonPressed = false;
            chrome.browserAction.setIcon({ path: { 16: "assets/img/icon16-gray.png" } });
            sendResponse(tabsinfo[message.tabId]);
            break;
        case 'scan-completed':
            let res = message.result;
            createTabInfoIfDoesntExist(sender.tab.id);
            tabsinfo[sender.tab.id] = Object.assign({}, res, { showButtonPressed: tabsinfo[sender.tab.id].showButtonPressed, url: (new URL(document.location.href)).hostname });
            chrome.browserAction.setBadgeText({ text: res.foundElemsCount.toString(), tabId: sender.tab.id });
            sendResponse(tabsinfo[sender.tab.id]);
            break;
        case 'show-button-pressed':
            let tabid = message.tabId === undefined ? sender.tab.id : message.tabId;
            createTabInfoIfDoesntExist(tabid);
            tabsinfo[tabid].showButtonPressed = message.state;
            sendResponse(tabsinfo[tabid]);
            break;
        case 'login-google':
            let url = 'https://accounts.google.com/o/oauth2/v2/auth' +
                '?client_id=' + encodeURIComponent('457312517099-90qhv4vtncd8cmpp95a2hmtheo4lad5f.apps.googleusercontent.com') +
                '&response_type=' + encodeURIComponent('id_token') +
                '&redirect_uri=' + encodeURIComponent('https://napdgmfnfbebjgahggnalabkkfaajldf.chromiumapp.org') +
                '&state=' + encodeURIComponent('xxxx') +
                '&scope=' + encodeURIComponent('openid') +
                '&prompt=' + encodeURIComponent('consent') +
                '&nonce=' + encodeURIComponent(Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15));
            chrome.identity.launchWebAuthFlow({
                url: url, interactive: true
            }, function (url) {
                let idtoken = url.substring(url.indexOf('id_token=') + 9);
                idtoken = idtoken.substr(0, idtoken.indexOf('&'));
                API.googleLogin(idtoken).then(res => {
                    chrome.storage.local.set({ loggedUser: res });
                });
            });
            sendResponse('ok');
            break;
        case 'get-current-tab-info':
            createTabInfoIfDoesntExist(message.tabId === undefined ? sender.tab.id : message.tabId);
            sendResponse(tabsinfo[message.tabId === undefined ? sender.tab.id : message.tabId]);
            break;
    }
});
chrome.tabs.onActivated.addListener(function (info) {
    chrome.tabs.query({ active: true, currentWindow: true }, function (tabs) {
        chrome.storage.local.get('isOn', function (data) {
            createTabInfoIfDoesntExist(tabs[0].id);
            if (data.isOn && !tabsinfo[tabs[0].id].showButtonPressed) {
                var message = { name: 'refresh' };
                chrome.tabs.sendMessage(tabs[0].id, message);
            }
        });
    });
});
function createTabInfoIfDoesntExist(tabId) {
    if (tabsinfo[tabId] == undefined) {
        tabsinfo[tabId] = {
            cookies: 0,
            push: 0,
            subscribe: 0,
            adblocker: 0,
            other: 0,
            foundElemsCount: 0,
            showButtonPressed: false,
            url: (new URL(document.location.href)).hostname
        };
    }
}
function getNewRuleCache(oldRuleCache, apiFoundRulesets, url, timestamp) {
    let newRuleCache = oldRuleCache.filter(ruleset => {
        if (timestamp - ruleset.cached < Consts.CACHE_TIME)
            return true;
        return false;
    });
    newRuleCache.unshift({
        for: url,
        rulesets: apiFoundRulesets,
        cached: timestamp
    });
    newRuleCache = newRuleCache.slice(0, Consts.CACHE_MAX);
    return newRuleCache;
}
