chrome.storage.local.get(['isOn', 'miniWidget'], function (data) {
    chrome.runtime.sendMessage({ name: 'get-current-tab-info' }, (tabinfo) => {
        if (data.isOn) {
            if (tabinfo.showButtonPressed === false) {
                scanner.start();
            }
        }
        if (data.miniWidget.isOn) {
            if (data.miniWidget.alwaysOn ||
                (tabinfo.foundElemsCount > 0 && tabinfo.url === (new URL(document.location.href)).hostname)) {
                miniwidget.init(data.miniWidget);
            }
        }
        chrome.runtime.sendMessage({
            name: 'sync-rules',
            url: (new URL(document.location.href)).hostname.replace('www.', '')
        }, function (response) { });
    });
});
chrome.runtime.onMessage.addListener(function (request, sender, sendResponse) {
    switch (request.name) {
        case 'turn-on':
            scanner.start();
            miniwidget.initialised ? miniwidget.setStarted() : null;
            break;
        case 'turn-off':
            scanner.stop();
            miniwidget.initialised ? miniwidget.setStopped() : null;
            break;
        case 'refresh':
            scanner.isRunning() ? null : scanner.executeScanIteration();
            break;
        case 'show-button-pressed':
            if (request.state === true) {
                scanner.stop();
                miniwidget.setShowFoundElems();
            }
            else {
                scanner.start();
                miniwidget.setHideFoundElems();
            }
            break;
        case 'popup-picker-pressed':
            picker.show();
            break;
        case 'rules-resolved':
            scanner.pageSpecificRules = request.rules;
            scanner.rulesUpdated = true;
            chrome.storage.local.get('isOn', function (data) {
                if (data.isOn) {
                    chrome.runtime.sendMessage({ name: 'get-current-tab-info' }, (tabinfo) => {
                        if (scanner.isRunning() === false && tabinfo.showButtonPressed === false) {
                            scanner.executeScanIteration();
                        }
                    });
                }
            });
            break;
        case 'mw-settings-onoff':
            if (request.value) {
                chrome.storage.local.get('miniWidget', (mw) => {
                    chrome.runtime.sendMessage({ name: 'get-current-tab-info' }, (tabinfo) => {
                        (mw.alwaysOn || tabinfo.foundElemsCount > 0) ? miniwidget.show() : null;
                    });
                });
            }
            else {
                miniwidget.hide();
            }
            break;
        case 'mw-settings-only':
            chrome.storage.local.get('miniWidget', (res) => {
                chrome.runtime.sendMessage({ name: 'get-current-tab-info' }, (tabinfo) => {
                    if (request.value) {
                        (res.miniWidget.isOn) ? miniwidget.show() : null;
                    }
                    else {
                        (res.miniWidget.isOn && tabinfo.foundElemsCount > 0) ? miniwidget.show() : miniwidget.hide();
                    }
                });
            });
            break;
    }
    sendResponse('response from content');
});
