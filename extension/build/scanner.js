const scanner = {
    maxRepeat: 3,
    iteration: 0,
    process: null,
    observer: null,
    showAllPressed: false,
    pageSpecificRules: [],
    rulesUpdated: false,
    unwantedElements: [],
    modifiedElements: [],
    lastResult: {
        foundElemsCount: 0, cookies: 0, push: 0, subscribe: 0, adblocker: 0, other: 0
    },
    start() {
        scanner.showAllPressed = false;
        scanner.hideFoundElems();
        scanner.observer = new MutationObserver(function (mutations) {
            if (!scanner.isRunning()) {
                scanner.process = setInterval(scanner.initScanIteration, 400);
            }
        });
        scanner.observer.observe(document.documentElement, {
            attributes: true,
            characterData: false,
            childList: true,
            subtree: true,
            attributeOldValue: false,
            characterDataOldValue: false
        });
        setTimeout(() => {
            if (!scanner.isRunning()) {
                scanner.process = setInterval(scanner.initScanIteration, 400);
            }
        }, 0);
    },
    stop() {
        scanner.showFoundElems();
        scanner.observer.disconnect();
        scanner.observer = null;
        clearInterval(scanner.process);
        scanner.process = null;
        scanner.iteration = 0;
    },
    isRunning() {
        return (scanner.process !== null);
    },
    initScanIteration() {
        if (scanner.maxRepeat <= scanner.iteration) {
            if (scanner.rulesUpdated) {
                scanner.iteration++;
                scanner.executeScanIteration();
            }
            else {
                clearInterval(scanner.process);
                scanner.process = null;
                scanner.iteration = 0;
            }
        }
        else {
            scanner.iteration++;
            scanner.executeScanIteration();
        }
    },
    executeScanIteration() {
        scanner.rulesUpdated = false;
        scanner.scanForSelectors();
        scanner.checkNestedElements();
        scanner.validateElements();
        scanner.processElements();
        scanner.checkScrollLock();
        scanner.uncacheInvalidElements();
        let foundEls = scanner.unwantedElements.filter(el => el.processed === true);
        console.log('found', foundEls);
        let res = {
            foundElemsCount: foundEls.length,
            cookies: foundEls.filter(el => el.foundByRules.filter(rule => typeof rule.type == 'undefined' || rule.type == 'cookies').length > 0).length,
            push: foundEls.filter(el => el.foundByRules.filter(rule => rule.type == 'push').length > 0).length,
            subscribe: foundEls.filter(el => el.foundByRules.filter(rule => rule.type == 'subscribe').length > 0).length,
            adblocker: foundEls.filter(el => el.foundByRules.filter(rule => rule.type == 'adblocker').length > 0).length,
            other: foundEls.filter(el => el.foundByRules.filter(rule => rule.type == 'other').length > 0).length,
        };
        chrome.runtime.sendMessage({
            name: 'scan-completed',
            result: res
        }, (res) => { });
        miniwidget.onScanCompleted(res);
    },
    scanForSelectors() {
        let url = window.location.hostname;
        WEBSITES.forEach(website => {
            if (url.indexOf(website.name) > -1) {
                website.selectors.forEach(rule => { scanner.searchBySelector(rule, 'specific'); });
            }
        });
        if (scanner.pageSpecificRules) {
            scanner.pageSpecificRules.forEach(ruleset => {
                ruleset.rules.forEach(r => {
                    if (r.selector === '*') {
                        scanner.executeRuleInstructions(r);
                    }
                    else {
                        scanner.searchBySelector(r, 'specific');
                    }
                });
            });
        }
        COMMON_RULES.forEach(rule => { scanner.searchBySelector(rule, 'common'); });
    },
    searchBySelector(rule, type) {
        let allowedElements = type == 'common' ?
            ':is(div, span, section, article, aside, footer, figure, p, table, dialog):not([data-provision])' :
            ':not(body):not(html):not(form):not(a):not(button):not(input):not(textarea):not(select):not(i):not(svg):not(g)' +
                ':not(path):not(circle):not(polygon):not(h1):not(h2):not(h3):not(h4):not(h5):not(ul):not(ol):not(li):not(img)' +
                ':not(script):not(link):not(meta):not(style)' +
                ':not([data-provision])';
        let foundDomEls = document.querySelectorAll(rule.selector + allowedElements);
        [...foundDomEls].forEach((el) => {
            let foundByRule = Object.assign({}, rule, { type: rule.type == undefined ? 'cookies' : rule.type, pageSpecific: type === 'specific' ? true : false, valid: false });
            let existingEl = scanner.unwantedElements.find(u => u.element == el);
            if (existingEl === undefined) {
                el.setAttribute('data-provision', 'unwanted');
                scanner.unwantedElements.push({
                    element: el,
                    nested: null,
                    valid: null,
                    processed: null,
                    originalDisplay: el.style.display,
                    observer: null,
                    foundByRules: [foundByRule],
                    executedRules: []
                });
            }
            else {
                existingEl.foundByRules.push(foundByRule);
            }
        });
    },
    checkNestedElements() {
        let nested = document.querySelectorAll('[data-provision="unwanted"] [data-provision="unwanted"]');
        [...nested].forEach(n => {
            scanner.unwantedElements.forEach(u => {
                if (n == u.element) {
                    n.setAttribute('data-provision', 'nested');
                    u.nested = true;
                }
            });
        });
    },
    validateElements() {
        scanner.unwantedElements.forEach((uel) => {
            if (uel.processed !== true && uel.nested !== true && uel.valid !== true && scanner.isVisible(uel.element)) {
                uel.valid = false;
                uel.foundByRules.forEach((foundByRule, ind) => {
                    if (validator.validateMustContain(uel, ind)) {
                        foundByRule.valid = true;
                        uel.valid = true;
                    }
                    else {
                        if (foundByRule.type === 'cookies') {
                            if (foundByRule.pageSpecific === false || (foundByRule.pageSpecific && foundByRule.validate)) {
                                if (validator.validateCookies(uel, ind)) {
                                    foundByRule.valid = true;
                                    uel.valid = true;
                                }
                            }
                        }
                        else {
                            foundByRule.valid = true;
                            uel.valid = true;
                        }
                    }
                });
            }
        });
    },
    processElements() {
        if (!scanner.showAllPressed) {
            scanner.unwantedElements.forEach((uel) => {
                if (uel.valid === true && uel.processed !== true) {
                    uel.originalDisplay = uel.element.style.display;
                    uel.element.style.setProperty('display', 'none', 'important');
                    scanner.executeAllRulesInstructions(uel);
                    uel.processed = true;
                    uel.observer = new MutationObserver((muts) => {
                        if (!scanner.showAllPressed && muts.find(m => m.attributeName === 'style') !== undefined) {
                            uel.element.style.setProperty('display', 'none', 'important');
                        }
                    });
                    uel.observer.observe(uel.element, { attributes: true });
                }
            });
        }
    },
    executeAllRulesInstructions(unwantedEl) {
        unwantedEl.foundByRules.forEach((rule, i) => {
            if (unwantedEl.executedRules.indexOf(i) == -1) {
                scanner.executeRuleInstructions(rule);
            }
            unwantedEl.executedRules.push(i);
        });
    },
    executeRuleInstructions(rule) {
        if (rule.setStyle !== undefined) {
            rule.setStyle.forEach(ss => {
                let mEl = document.querySelector(ss.for);
                if (mEl) {
                    let existingMEl = scanner.modifiedElements.find(el => mEl == el.element);
                    if (existingMEl === undefined) {
                        scanner.modifiedElements.push({
                            element: mEl,
                            originalStyle: mEl.style.cssText,
                            rules: [ss],
                        });
                    }
                    else {
                        existingMEl.rules.push(ss);
                    }
                    mEl.setAttribute('data-provision', 'modified');
                    mEl.style.cssText = mEl.style.cssText + ' ' + ss.style;
                }
            });
        }
    },
    checkScrollLock() {
        if (scanner.unwantedElements.filter(u => u.processed === true).length > 0) {
            if (scanner.modifiedElements.find(m => m.element == document.documentElement) == undefined) {
                let htmlstyle = getComputedStyle(document.documentElement);
                if (htmlstyle.overflowY == 'hidden') {
                    scanner.modifiedElements.push({
                        element: document.documentElement,
                        originalStyle: document.documentElement.style.cssText,
                        rules: ['scroll']
                    });
                    document.documentElement.style.setProperty('overflow-y', 'auto', 'important');
                    document.documentElement.style.setProperty('position', 'relative', 'important');
                    document.documentElement.style.setProperty('min-height', '100vh', 'important');
                }
            }
            if (scanner.modifiedElements.find(m => m.element == document.body) == undefined) {
                let bodystyle = getComputedStyle(document.body);
                if (bodystyle.overflowY == 'hidden') {
                    scanner.modifiedElements.push({
                        element: document.body,
                        originalStyle: document.body.style.cssText,
                        rules: ['scroll']
                    });
                    document.body.style.setProperty('overflow-y', 'auto', 'important');
                    document.body.style.setProperty('position', 'relative', 'important');
                    document.body.style.setProperty('min-height', '100vh', 'important');
                }
            }
        }
    },
    uncacheInvalidElements() {
        scanner.unwantedElements = scanner.unwantedElements.filter(el => document.body.contains(el.element));
        scanner.unwantedElements = scanner.unwantedElements.filter(el => {
            if (el.valid) {
                let parentEl = scanner.unwantedElements.find(e => e.valid && e !== el && e.element.contains(el.element));
                if (parentEl !== undefined) {
                    console.log('child:', el);
                    console.log('parent:', parentEl);
                    console.log('setting display:', parentEl);
                    el.element.style.display = (el.originalDisplay == 'none' ? 'auto' : el.originalDisplay);
                    return false;
                }
                else
                    return true;
            }
            else
                return true;
        });
    },
    isVisible(elem) {
        const style = getComputedStyle(elem);
        if (style.display === 'none')
            return false;
        if (style.visibility !== 'visible')
            return false;
        if (parseFloat(style.opacity) < 0.1)
            return false;
        if (typeof elem.getBoundingClientRect === 'function') {
            let dims = elem.getBoundingClientRect();
            if (dims.width > 0 || dims.height > 0) {
                return true;
            }
            let allchilds = elem.querySelectorAll('*');
            for (let i = 0; i < allchilds.length; i++) {
                if (typeof allchilds[i].getBoundingClientRect === 'function') {
                    let dims = allchilds[i].getBoundingClientRect();
                    if (dims.width > 0 || dims.height > 0) {
                        return true;
                    }
                }
            }
        }
        return false;
    },
    showFoundElems() {
        scanner.showAllPressed = true;
        scanner.unwantedElements.filter(uel => uel.processed === true).forEach(el => {
            el.element.style.display = (el.originalDisplay == 'none' ? 'auto' : el.originalDisplay);
        });
        scanner.modifiedElements.forEach(el => {
            el.element.style.cssText = el.originalStyle;
        });
    },
    hideFoundElems() {
        scanner.showAllPressed = false;
        scanner.unwantedElements.filter(uel => !uel.nested && uel.valid).forEach((uel) => {
            uel.element.style.setProperty('display', 'none', 'important');
        });
        scanner.modifiedElements.forEach(mel => {
            mel.rules.forEach(rule => {
                if (rule == 'scroll') {
                    mel.element.style.setProperty('overflow-y', 'auto', 'important');
                    mel.element.style.setProperty('position', 'relative', 'important');
                    mel.element.style.setProperty('min-height', '100vh', 'important');
                }
                else if (rule.style) {
                    mel.element.style.cssText = mel.element.style.cssText + ' ' + rule.style;
                }
            });
        });
    },
};
