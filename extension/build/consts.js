const Consts = {
    ENV: 'prod',
    SECRET: '5d61re5g4r6ef165rh1wrf6d5a',
    URL_FB: "https://www.facebook.com/axelproductions86",
    URL_WEB: "https://www.axelproductions86.com",
    URL_EDITOR: "https://www.axelproductions86.com/provision-rule-editor",
    URL_API_PROD: "https://www.axelproductions86.com/api",
    URL_API_LOC: "http://axelproductions86.loc/api",
    URL_REGISTER: "https://www.axelproductions86.com/provision-rule-editor?page=register",
    URL_RECOVERY: "https://www.axelproductions86.com/provision-rule-editor?page=recovery",
    CLIENT_TYPE: 2,
    CACHE_TIME: 1000 * 60 * 60 * 24,
    CACHE_MAX: 300,
    ERROR_CODES: {
        SOMETHING_WENT_WRONG: '0',
        WRONG_USERNAME_PASSOWRD: '1',
        USER_EXISTS: '2',
        ACCOUNT_NOT_ACTIVATED: '3',
        WRONG_PASSOWRD: '4',
        WRONG_EMAIL: '5',
        RULESET_NAME_EXISTS: '6',
        RULESET_INVALID_NAME: '7',
        RULESET_INVALID_JSON: '8',
        RULESET_DOCUMENTATION_NOT_READED: '9',
        ACCESS_DENIED: '10',
        UNSUCCESSFUL_GOOGLE_LOGIN: '10',
    }
};
function cl($val1, $val2 = null) {
    if ($val2 == null) {
        console.log('(proVision) log: ', $val1);
    }
    else {
        console.log('(proVision) ' + $val1 + ': ', $val2);
    }
}
