let appEl = document.getElementById('app')
let versionEl = document.getElementById('version')

let loginOpener = document.getElementById('login')
let logoutButton = document.getElementById('logout')
let userButton = document.getElementById('username')
let mainButton = document.getElementById('mainbutton')
let settingsButton = document.getElementById('button-settings')
let feedbackButton = document.getElementById('button-feedback')

let showButton = document.getElementById('button-show')
let countEl = document.getElementById('count')
let indicatorCookies = document.getElementById('indicator-cookies')
let indicatorPush = document.getElementById('indicator-push')
let indicatorSubscribe = document.getElementById('indicator-subscribe')
let indicatorAdblocker = document.getElementById('indicator-adblocker')
let indicatorOther = document.getElementById('indicator-other')

let modeHideButton = document.getElementById('mode-hide')
let modeAcceptButton = document.getElementById('mode-accept')
let modeRejectButton = document.getElementById('mode-reject')

let pickerElem = document.querySelector('.picker')
let pickerButton = document.getElementById('button-picker')

let fbButton = document.getElementById('link-fb')
let webButton = document.getElementById('link-web')

let feedbackPanel = document.getElementById('feedback-panel')
let feedbackUrl = document.getElementById('feedback-url') as HTMLInputElement
let feedbackEmail = document.getElementById('feedback-email') as HTMLInputElement
let feedbackText = document.getElementById('feedback-text') as HTMLInputElement
let feedbackSendButton = document.getElementById('btn-send-feedback')
let feedbackOpenRuleEditor = document.getElementById('btn-open-rule-editor')

let settingsPanel = document.getElementById('settings-panel')
let settingsClearButton = document.getElementById('btn-clear-cache')
let settingsCacheCount = document.getElementById('cache-count')
let settingsMiniwidgetShow = document.getElementById('mw-onoff') as HTMLInputElement
let settingsMiniwidgetOnly = document.getElementById('mw-only') as HTMLInputElement

let closeLogin = document.getElementById('btn-close-login')
let loginButton = document.getElementById('login-submit')
let loginGoogleButton = document.getElementById('g-signin2')
let loginEmail = document.getElementById('login-email') as HTMLInputElement
let loginPwd = document.getElementById('login-pwd') as HTMLInputElement
let loginRegister = document.getElementById('login-register') as HTMLInputElement
let loginReset = document.getElementById('login-reset') as HTMLInputElement
let loginErrPwdName = document.getElementById('login-err-pwdname') as HTMLInputElement
let loginErrNotActivated = document.getElementById('login-err-notactivated') as HTMLInputElement


/**
 * CHROME - on popup open
 * - get storage.isOn status and set popup UI accordingly
 */
window.onload = function() {
  setUI(true);
}

/**
 * On/off button click
 */
mainButton.onclick = function(element) {
  chrome.storage.local.get(['isOn', 'loggedUser'], (data) => {
    chrome.storage.local.set({ isOn: !data.isOn }, () => {
      setUI(false);      
      // send message to active tab and background
      chrome.tabs.query({active: true, currentWindow: true}, function (tabs) {
        var message = data.isOn ? { name: 'turn-off', tabId: tabs[0].id } : { name: 'turn-on', tabId: tabs[0].id };
        chrome.tabs.sendMessage(tabs[0].id, message);
        chrome.runtime.sendMessage(message);
      });
    });
  });
};

/**
 * SHOW ALL button click
 */ 
showButton.onclick = function(el) {
  chrome.storage.local.get('isOn', function (data) {
    // only if extension is ON
    if (data.isOn) {
      let newState = showButton.classList.contains('pressed') ? false : true;
      chrome.tabs.query({active: true, currentWindow: true}, (tabs) => { 
        chrome.runtime.sendMessage({ 
          name: 'show-button-pressed', tabId: tabs[0].id, state: newState
        }, tabinfo => { 
          showButton.classList.toggle('pressed', newState);
          var message = { name: 'show-button-pressed', state: newState };
          chrome.tabs.sendMessage(tabs[0].id, message);
        })
      });
    }
  });
}

/**
 * POPUP PICKER button click
 */
pickerButton.onclick = (ev) => {
  chrome.storage.local.get('isOn', data => {
    data.isOn ? chrome.tabs.query({active: true, currentWindow: true}, (tabs) => { 
      chrome.tabs.sendMessage(tabs[0].id, {
        name: 'popup-picker-pressed'
      }, tabinfo => { 
        window.close()
      })
    }) : null;
  })
}
pickerButton.onmouseenter = (ev) => {
  chrome.tabs.query({active: true, currentWindow: true}, (tabs) => { 
    pickerElem.classList.add('hover')
  })  
}
pickerButton.onmouseleave = (ev) => {
  pickerElem.classList.remove('hover')
}

/**
 * Open/close settings panel
 */ 
settingsButton.onclick = function(el) {
  if (!appEl.classList.contains('settings-opened')) {
    appEl.classList.toggle('settings-opened', true);
    settingsButton.setAttribute('title', chrome.i18n.getMessage('close'))
    chrome.storage.local.get(['pageSpecificRuleCache', 'miniWidget'], function(data) { 
      settingsCacheCount.innerHTML = data.pageSpecificRuleCache.length
      settingsMiniwidgetShow.checked = data.miniWidget.isOn
      settingsMiniwidgetOnly.checked = !data.miniWidget.alwaysOn
    })
  } else {
    appEl.classList.toggle('settings-opened', false);
    settingsButton.setAttribute('title', chrome.i18n.getMessage("settingsBtnTitle"))
  }

}

/**
 * Open/close feedback panel
 */ 
feedbackButton.onclick = function(el) {
  if (!appEl.classList.contains('feedback-opened')) {
    // set last used user email
    chrome.storage.local.get(['loggedUser','userEmail'], function(data) {
      feedbackEmail.value = data.loggedUser !== null ? 
        (data.loggedUser as LoggedUser).user.email : 
        (data.userEmail !== undefined ? data.userEmail : '')
    })
    // set current tab url 
    chrome.tabs.query({active: true, currentWindow: true}, tabs => {
      appEl.classList.toggle('feedback-opened')
      feedbackButton.setAttribute('title', chrome.i18n.getMessage("close"))
      feedbackUrl.value = (new URL(tabs[0].url)).hostname
    })
  } else {
    appEl.classList.toggle('feedback-opened')
    feedbackButton.setAttribute('title', chrome.i18n.getMessage("feedbackBtnTitle"))
  }
}

/**
 * Send feedback button
 */
feedbackSendButton.onclick = function(el) {
  // eliminate multiple clicks 
  if(!feedbackPanel.classList.contains('sending')) {

    let valid = true;
    if (feedbackUrl.value.length < 3) { feedbackUrl.classList.toggle('error', true); valid = false; } 
    else { feedbackUrl.classList.toggle('error', false); }
    if (feedbackText.value.length < 3) { feedbackText.classList.toggle('error', true); valid = false; } 
    else { feedbackUrl.classList.toggle('error', false); }

    chrome.storage.local.set({ userEmail: feedbackEmail.value })

    if (valid) {
      chrome.storage.local.get('loggedUser', (res) => {
        API.sendFeedback({ 
          userid: res.loggedUser !== null ? (res.loggedUser as LoggedUser).user.id : feedbackEmail.value, 
          url: feedbackUrl.value, 
          text: feedbackText.value
        });
        // close panel animation
        feedbackPanel.classList.toggle('sending', true);
        let t = setTimeout(() => {
          appEl.classList.toggle('feedback-opened', false);
          feedbackButton.setAttribute('title', chrome.i18n.getMessage("feedbackBtnTitle"))
          let v = setTimeout(() => {
            feedbackPanel.classList.toggle('sending', false);
          }, 500)
        }, 1000) 

      })
    }
  }
}

/**
 * Clear Cache button click
 */
settingsClearButton.onclick = function(el) {
  chrome.storage.local.set({ pageSpecificRuleCache: [] })
  chrome.storage.local.get(['pageSpecificRuleCache'], function(data) {
    settingsCacheCount.innerHTML = data.pageSpecificRuleCache.length
  })
}

/**
 * Miniwidget settings ON/OFF
 */
settingsMiniwidgetShow.onchange = function(ev) {
  chrome.storage.local.get('miniWidget', (data) => {
    chrome.storage.local.set({ miniWidget: { ...data.miniWidget, isOn: settingsMiniwidgetShow.checked }}, () => {
      chrome.tabs.query({}, (tabs) => {
        tabs.forEach((tab) => {
          chrome.tabs.sendMessage(tab.id, { name: 'mw-settings-onoff', value: settingsMiniwidgetShow.checked})
        })
      }) 
    })
  })
}

/**
 * Miniwidget settings Always on
 */
settingsMiniwidgetOnly.onchange = function(ev) {
  chrome.storage.local.get('miniWidget', (data) => {
    chrome.storage.local.set({ miniWidget: { ...data.miniWidget, alwaysOn: !settingsMiniwidgetOnly.checked }}, () => {
      chrome.tabs.query({}, (tabs) => {
        tabs.forEach((tab) => {
          chrome.tabs.sendMessage(tab.id, { name: 'mw-settings-only', value: !settingsMiniwidgetOnly.checked})
        })
      }) 
    })
  })
}

/**
 * Open login panel
 */
loginOpener.onclick = function(el) {
  appEl.classList.add('login-opened')
  appEl.classList.remove('feedback-opened')
  appEl.classList.remove('settings-opened')
  loginButton.classList.remove('loading')
  loginButton.classList.remove('ok')
}

closeLogin.onclick = function(el) {
  appEl.classList.remove('login-opened')
}

loginButton.onclick = function(el) {
  if (!loginButton.classList.contains('loading') && !loginButton.classList.contains('ok')) {

    loginErrPwdName.classList.add('hidden')
    loginErrNotActivated.classList.add('hidden')
    loginButton.classList.add('loading')
    API.login(loginEmail.value, loginPwd.value).then(res => {
      cl('login', res)
      loginButton.classList.remove('loading')
      if (res.error) {
        switch ((res.error as Err).code) {
          case Consts.ERROR_CODES.WRONG_USERNAME_PASSOWRD: loginErrPwdName.classList.remove('hidden'); break
          case Consts.ERROR_CODES.ACCOUNT_NOT_ACTIVATED: loginErrNotActivated.classList.remove('hidden'); break
        }      
      } else {
        chrome.storage.local.set({ loggedUser: res })
        loginButton.classList.add('ok')
        appEl.classList.add('logged-in')
        userButton.innerHTML = res.user.nickname
        closeLogin.click()
      }
    })
  }
}

logoutButton.onclick = function(el) {
  chrome.storage.local.set({ loggedUser: null })
  appEl.classList.remove('logged-in')
}

loginRegister.onclick = function(el) {
  chrome.tabs.create({active: true, url: Consts.URL_REGISTER});
}
loginReset.onclick = function(el) {
  chrome.tabs.create({active: true, url: Consts.URL_RECOVERY});
}

loginGoogleButton.addEventListener('click', function() {
  chrome.runtime.sendMessage({ 
    name: 'login-google'
  }, res => { })
});

/**
 * Links
 */ 
fbButton.onclick = function(el) {
  chrome.tabs.create({active: true, url: Consts.URL_FB});
};
webButton.onclick = function(el) {
  chrome.tabs.create({active: true, url: Consts.URL_WEB});
};
feedbackOpenRuleEditor.onclick = function(el) {
  chrome.tabs.query({active: true, currentWindow: true}, tabs => {
    chrome.tabs.create({active: true, url: Consts.URL_EDITOR + '?find=' + (new URL(tabs[0].url)).hostname});
  })
};


/**
 * Listen to messages
 */
chrome.runtime.onMessage.addListener(
  function(message, sender, sendResponse) {
    switch (message.name) {
      // from scanner.js, on iteration complete
      case 'scan-completed': 
        let res = message.result as scanCompletedResult;
        // if scan result is coming from active tab only
        if (sender.tab.active) {
          countEl.innerHTML = res.foundElemsCount > 9 ? "9" : res.foundElemsCount.toString();
          indicatorCookies.classList.toggle('active', res.cookies > 0);
          indicatorPush.classList.toggle('active', res.push > 0);
          indicatorSubscribe.classList.toggle('active', res.subscribe > 0);
          indicatorAdblocker.classList.toggle('active', res.adblocker > 0);
          indicatorOther.classList.toggle('active', res.other > 0);
        }
        break;
    }

    sendResponse('ok');
  }
);

/**
 * Set popup content according to state from storage
 **/ 
function setUI(opened = false) {
  chrome.storage.local.get(['isOn', 'loggedUser'], function (data) {
    if (data.isOn) {
      appEl.classList.add('on')
      appEl.classList.remove('off')
      //chrome.browserAction.setIcon({ path: { 16: "assets/img/icon16.png" }});
      anim.animWorking(true);
      opened ? null : anim.animTurnOn();
      setUIByTabInfo();

    } else {
      appEl.classList.add('off')
      appEl.classList.remove('on')
      showButton.classList.remove('pressed');
      //chrome.browserAction.setIcon({ path: { 16: "assets/img/icon16-gray.png" }});
      anim.animWorking(false);
      anim.animTurnOff();
    }

    if (opened) {
      if (data.loggedUser !== null) {
        userButton.innerHTML = (data.loggedUser as LoggedUser).user.nickname
        feedbackEmail.value = (data.loggedUser as LoggedUser).user.email
        appEl.classList.add('logged-in')
      } 
      versionEl.innerHTML = chrome.runtime.getManifest().version;
      setLanguage();
    }
  })

};

/**
 * Get current tab info and set elements accordingly
 */
function setUIByTabInfo() {
  chrome.tabs.query({active: true, currentWindow: true}, (tabs) => { 
    chrome.runtime.sendMessage({ 
      name: 'get-current-tab-info', tabId: tabs[0].id
    }, tabinfo => { 
      //currentTabInfo = tabinfo;
      // sometimes el is undefined for some reasons
      if (countEl) {
        countEl.innerHTML = tabinfo.foundElemsCount > 9 ? 9 : tabinfo.foundElemsCount;
        showButton.classList.toggle('pressed', tabinfo.showButtonPressed);
        indicatorCookies.classList.toggle('active', tabinfo.cookies > 0);
        indicatorPush.classList.toggle('active', tabinfo.push > 0);
        indicatorSubscribe.classList.toggle('active', tabinfo.subscribe > 0);
        indicatorAdblocker.classList.toggle('active', tabinfo.adblocker > 0);
        indicatorOther.classList.toggle('active', tabinfo.other > 0);
      }
    })
  })
}

/**
 * Set elements texts according to current language
 */
function setLanguage() {
 
  document.querySelector('#onoff .off').innerHTML = chrome.i18n.getMessage("itsOff")
  document.querySelector('#onoff .on').innerHTML = chrome.i18n.getMessage("itsOn")  
  document.querySelector('.hidden-elements .header .bold').innerHTML = chrome.i18n.getMessage("hiddenElementsTitle")
  document.querySelector('.hidden-elements .header .text').innerHTML = chrome.i18n.getMessage("hiddenElementsText")
  document.querySelector('.hidden-elements .cont .text').innerHTML = chrome.i18n.getMessage("hiddenElementsFoundText")
  document.querySelector('.hidden-elements #button-show span').innerHTML = chrome.i18n.getMessage("hiddenElementsButton")
  document.querySelector('.mode .header .bold').innerHTML = chrome.i18n.getMessage("cookieModeTitle")
  document.querySelector('.mode .header .text').innerHTML = chrome.i18n.getMessage("cookieModeText1")
  //document.querySelector('.mode .header .text2 span').innerHTML = chrome.i18n.getMessage("cookieModeText2")
  document.querySelector('.mode #mode-hide').innerHTML = chrome.i18n.getMessage("cookieModeHide")
  document.querySelector('.mode #mode-accept span').innerHTML = chrome.i18n.getMessage("cookieModeAccept")
  document.querySelector('.mode #mode-reject span').innerHTML = chrome.i18n.getMessage("cookieModeReject")
  document.querySelector('.picker .header .text1').innerHTML = chrome.i18n.getMessage("pickerTextMain")
  document.querySelector('.picker .header .text2').innerHTML = chrome.i18n.getMessage("pickerTextHover")
  // document.querySelector('.textblock .text').innerHTML = chrome.i18n.getMessage("feedbackMessage")
  document.querySelector('.feedbackpanel .title').innerHTML = chrome.i18n.getMessage("feedbackTitle")
  document.querySelector('.feedbackpanel .text').innerHTML = chrome.i18n.getMessage("feedbackText")
  document.querySelector('.feedbackpanel .label.url').innerHTML = chrome.i18n.getMessage("feedbackUrlLabel")
  document.querySelector('.feedbackpanel .label.email').innerHTML = chrome.i18n.getMessage("feedbackEmailLabel")
  document.querySelector('.feedbackpanel .label.message').innerHTML = chrome.i18n.getMessage("feedbackMessageLabel")
  document.querySelector('.feedbackpanel #btn-send-feedback > span:first-child span').innerHTML = chrome.i18n.getMessage("feedbackSend")
  document.querySelector('.feedbackpanel #btn-send-feedback > span:last-child span').innerHTML = chrome.i18n.getMessage("feedbackSendThx")
  document.querySelector('.settingspanel .title').innerHTML = chrome.i18n.getMessage("settingsTitle")
  document.querySelector('.settingspanel .cache .bold').innerHTML = chrome.i18n.getMessage("settingsCacheTitle")
  document.querySelector('.settingspanel .cache .text').innerHTML = chrome.i18n.getMessage("settingsCacheText")
  document.querySelector('.settingspanel .cache a span').innerHTML = chrome.i18n.getMessage("settingsCacheButton")
  document.querySelector('.settingspanel .cache .right .elements .text1').innerHTML = chrome.i18n.getMessage("settingsCacheCountText1")
  document.querySelector('.settingspanel .miniwidget .header .text').innerHTML = chrome.i18n.getMessage("settingsMvText")
  document.querySelector('.settingspanel .miniwidget label:first-child span').innerHTML = chrome.i18n.getMessage("settingsMvIsOnLabel")
  document.querySelector('.settingspanel .miniwidget label:last-child span').innerHTML = chrome.i18n.getMessage("settingsMvOnlyLabel")

  loginOpener.innerHTML = chrome.i18n.getMessage("login");
  logoutButton.innerHTML = chrome.i18n.getMessage("logout");
  mainButton.setAttribute('title', chrome.i18n.getMessage("onOff"))
  feedbackButton.setAttribute('title', chrome.i18n.getMessage("feedbackBtnTitle"))
  settingsButton.setAttribute('title', chrome.i18n.getMessage("settingsBtnTitle"))
  closeLogin.setAttribute('title', chrome.i18n.getMessage("close"))
  showButton.setAttribute('title', chrome.i18n.getMessage("hiddenElementsButtonTitle"))
  fbButton.setAttribute('title', chrome.i18n.getMessage("footerFBLink"))
  webButton.setAttribute('title', chrome.i18n.getMessage("footerWebLink"))
  feedbackSendButton.setAttribute('title', chrome.i18n.getMessage("feedbackSendTitle"))
  feedbackUrl.setAttribute('placeholder', chrome.i18n.getMessage("feedbackUrlPlaceholder"))
  feedbackEmail.setAttribute('placeholder', chrome.i18n.getMessage("feedbackEmailPlaceholder"))
  feedbackText.setAttribute('placeholder', chrome.i18n.getMessage("feedbackMessagePlaceholder"))
  feedbackOpenRuleEditor.setAttribute('title', chrome.i18n.getMessage("feedbackOpenRuleEditor"))
  modeHideButton.setAttribute('title', chrome.i18n.getMessage("cookieModeHideTitle"))
  modeAcceptButton.setAttribute('title', chrome.i18n.getMessage("notAvailableInBeta"))
  modeRejectButton.setAttribute('title', chrome.i18n.getMessage("notAvailableInBeta"))
  indicatorCookies.setAttribute('title', chrome.i18n.getMessage("indicatorCookies"))
  indicatorPush.setAttribute('title', chrome.i18n.getMessage("indicatorPush"))
  indicatorSubscribe.setAttribute('title', chrome.i18n.getMessage("indicatorSubscribe"))
  indicatorAdblocker.setAttribute('title', chrome.i18n.getMessage("indicatorAdblocker"))
  indicatorOther.setAttribute('title', chrome.i18n.getMessage("indicatorOther"))


}