
/** 
 * Store for tabs informations
 */ 
let tabsinfo: tabsInfo = {}


/**
 * CHROME - on extension install
 */
chrome.runtime.onInstalled.addListener(function () {
  chrome.storage.local.set({ 
    isOn: true,
    loggedUser: null,
    pageSpecificRuleCache: [],
    miniWidget: { 
      isOn: true, alwaysOn: false, positionX: -1 
    },
  }, function () {
    console.log("(proVision) proVision Chrome extension: up & running");
    chrome.browserAction.setBadgeBackgroundColor({color: '#555555'});
  });
});

/**
 * Listen to messages
 */
chrome.runtime.onMessage.addListener(
  function(message, sender, sendResponse) {

    switch (message.name) {

      /**
       * From content.js (on url opened), set cached or synced rules
       */
      case 'sync-rules':

        chrome.storage.local.get(['pageSpecificRuleCache'], function (data) { 

          // find valid local cached item for opened tab url
          let timestamp = (new Date()).getTime();
          let validLocalCachedItem = (data.pageSpecificRuleCache as RuleCache).find(ruleset => {
            if ((message.url == ruleset.for) && (timestamp - ruleset.cached < Consts.CACHE_TIME)) return true;
            return false;
          })

          // if no valid item is cached, 
          if (validLocalCachedItem === undefined) {
            // sync from server
            API.findRules(message.url).then(apiFoundRulesets => {
              // remove expired items from ruleCache and add new item
              let newRuleCache = getNewRuleCache(data.pageSpecificRuleCache, apiFoundRulesets, message.url, timestamp)
              // update local cached rules
              chrome.storage.local.set({ pageSpecificRuleCache: newRuleCache }, function () {
                // and send found rules from API to scanner
                chrome.tabs.sendMessage(sender.tab.id, { name: 'rules-resolved', rules: apiFoundRulesets });
              });
            }).catch(err => console.log(err))

          // otherwise use local cached rules
          } else {
            chrome.tabs.sendMessage(sender.tab.id, { name: 'rules-resolved', rules: validLocalCachedItem.rulesets });
          }
        });

        sendResponse(tabsinfo[sender.tab.id]);
        break;

      /**
       * From popup.js and widget.js, on mainbutton click
       * - reset showButtonPressed for current tab in tabsinfo
       * - set icon
       */ 
      case 'turn-on':
        tabsinfo[message.tabId === undefined ? sender.tab.id : message.tabId].showButtonPressed = false;
        chrome.browserAction.setIcon({ path: { 16: "assets/img/icon16.png" }});
        sendResponse(tabsinfo[message.tabId]);
        break;

      /**
       * From popup.js and widget.js, on mainbutton click
       * - reset showButtonPressed for current tab in tabsinfo
       * - set icon
       */ 
      case 'turn-off':
        tabsinfo[message.tabId === undefined ? sender.tab.id : message.tabId].showButtonPressed = false;
        chrome.browserAction.setIcon({ path: { 16: "assets/img/icon16-gray.png" }});
        sendResponse(tabsinfo[message.tabId]);
        break;

      /**
       * From scanner.js, after every iteration 
       * - store count of found elements for current tab into tabsinfo
       * - set extension icon badge text 
       */ 
      case 'scan-completed':
        // cl('scan completed', message.result)
        let res = message.result as scanCompletedResult;
        createTabInfoIfDoesntExist(sender.tab.id);
        tabsinfo[sender.tab.id] = { 
          ...res, 
          showButtonPressed: tabsinfo[sender.tab.id].showButtonPressed,
          url: (new URL(document.location.href)).hostname
        };
        chrome.browserAction.setBadgeText({ text: res.foundElemsCount.toString(), tabId: sender.tab.id });
        sendResponse(tabsinfo[sender.tab.id]);
        break;

      /**
       * From popup.js and miniwidget.js, on "show-all" button click
       * - set new state of show button for current tab in tabsinfo
       */
      case 'show-button-pressed':
        let tabid = message.tabId === undefined ? sender.tab.id : message.tabId
        createTabInfoIfDoesntExist(tabid);
        tabsinfo[tabid].showButtonPressed = message.state;
        sendResponse(tabsinfo[tabid]); 
        break;


      /**
       * From popup.js, on sign-in-with-google button click
       */
      case 'login-google':
        let url = 'https://accounts.google.com/o/oauth2/v2/auth' +
        '?client_id='+encodeURIComponent('457312517099-90qhv4vtncd8cmpp95a2hmtheo4lad5f.apps.googleusercontent.com')+
        '&response_type='+encodeURIComponent('id_token')+
        '&redirect_uri='+encodeURIComponent('https://napdgmfnfbebjgahggnalabkkfaajldf.chromiumapp.org')+
        '&state='+encodeURIComponent('xxxx')+
        '&scope='+encodeURIComponent('openid')+
        '&prompt='+encodeURIComponent('consent')+
        '&nonce='+encodeURIComponent(Math.random().toString(36).substring(2,15) + Math.random().toString(36).substring(2,15))

        chrome.identity.launchWebAuthFlow({ 
          url: url, interactive: true
        }, function(url) {
          let idtoken = url.substring(url.indexOf('id_token=') + 9)
          idtoken = idtoken.substr(0, idtoken.indexOf('&'))
          API.googleLogin(idtoken).then(res => {
            chrome.storage.local.set({ loggedUser: res })
          })
        });
        sendResponse('ok'); 
        break;

      /**
       * From popup.js or miniwidget.js
       * - return current tab info from tabinfo
       * - when called from popup, sender has no sender.tab.id property
       */
      case 'get-current-tab-info':
        createTabInfoIfDoesntExist(message.tabId === undefined ? sender.tab.id : message.tabId);
        sendResponse(tabsinfo[message.tabId === undefined ? sender.tab.id : message.tabId]); 
        break;
    }
  }
);

/**
 * CHROME - On tab switching
 * - send "refresh" message for scanner of current (switched onto) tab
 */
chrome.tabs.onActivated.addListener(function (info) {
  chrome.tabs.query({active: true, currentWindow: true}, function (tabs) {
    chrome.storage.local.get('isOn', function (data) {
      createTabInfoIfDoesntExist(tabs[0].id);
      if (data.isOn && !tabsinfo[tabs[0].id].showButtonPressed) {
        var message = { name: 'refresh' };
        chrome.tabs.sendMessage(tabs[0].id, message);
      }
    });
  });
})

/**
 * If given tabId is not created in tabsinfo yet this will create it.
 */
function createTabInfoIfDoesntExist(tabId) {
  if (tabsinfo[tabId] == undefined) {
    tabsinfo[tabId] = {
      cookies: 0,
      push: 0,
      subscribe: 0,
      adblocker: 0,
      other: 0, 
      foundElemsCount: 0,
      showButtonPressed: false,
      url: (new URL(document.location.href)).hostname
    } as tabInfo;
  } 
}

function getNewRuleCache(oldRuleCache, apiFoundRulesets, url, timestamp) {
  let newRuleCache = (oldRuleCache as RuleCache).filter(ruleset => {
    if (timestamp - ruleset.cached < Consts.CACHE_TIME) return true;
    return false;
  })
  
  // add new item to ruleCache
  newRuleCache.unshift({
    for: url,
    rulesets: apiFoundRulesets,
    cached: timestamp
  })

  // limit the pageSpecificRuleCache size to max X items
  newRuleCache = newRuleCache.slice(0, Consts.CACHE_MAX);
  return newRuleCache;
}