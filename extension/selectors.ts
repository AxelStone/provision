const COMMON_RULES: rule[] = [

  /* cookies */
  { selector: '[id^="cc-"]:not([id*="vp"])' },
  { selector: '[id^="cc_"]:not([id*="vp"])' },
  { selector: '[class^="cc-"]:not([class*="vp"])' },
  { selector: '[class^="cc_"]:not([class*="vp"])' },

  /*
  { selector: '[id*="rivacy"]' },
  { selector: '[class*="rivacy"]' },
  */

  // "privacy-banner"
  { selector: '[class*="rivacy"][class*="anner"]' },

  { selector: '[id*="onsent"]' },
  { selector: '[class*="onsent"]' },
  { selector: '[data-hook*="onsent"]' },

  { selector: '[aria-label*="ookie"]' },
  { selector: '[aria-describedby*="ookie"]' },
  { selector: '[id*="ookie"]' },
  { selector: '[class*="ookie"]:not(#page-container)' },
  { selector: '[name*="ookie"]' },
  { selector: '[data-test*="ookie"]' },
  { selector: '[data-testid*="ookie"]' },
  { selector: '[data-test-id*="ookie"]' },
  { selector: '[data-id*="ookie"]' },
  
  { selector: '[aria-label*="gdpr"]' },
  { selector: '[aria-describedby*="gdpr"]' },
  { selector: '[id*="gdpr"]' },
  { selector: '[class*="gdpr"]' },
  { selector: '[class*="Gdpr"]' },
  { selector: '[name*="gdpr"]' },

  { selector: '[aria-label*="GDPR"]' },
  { selector: '[aria-describedby*="GDPR"]' },
  { selector: '[id*="GDPR"]' },
  { selector: '[class*="GDPR"]' },
  { selector: '[name*="GDPR"]' },

  { selector: '[class*="ca_"]' },
  { selector: '[class*="ca-"]' },
  { selector: '[id*="cmp-"]', descr: 'Quantcast' },
  { selector: '[class*="cmp-"]', descr: 'Quantcast' },

  { selector: '[id*="optin-banner"]' },
  { selector: '[class*="optin-banner"]' },
  { selector: '[id*="shopify"][id*="popup"]', descr: 'Shopify' },
  { selector: '[class*="shopify"][class*="popup"]', descr: 'Shopify' },
  { selector: '[id*="optanon"]', descr: 'Optanon' },
  { selector: '[class*="optanon"]', descr: 'Optanon' },  
  { selector: '[data-hook*="optanon"]', descr: 'Optanon' },  
  { selector: '[id*="didomi"]', descr: 'DIDOMI' },
  { selector: '[class*="didomi"]', descr: 'DIDOMI' },   
  { selector: '[id*="evidon"]', descr: 'Evidon' },
  { selector: '[class*="evidon"]', descr: 'Evidon' },   
  { selector: '[id*="truste"]', descr: 'TrustArc' },
  { selector: '[class*="truste"]', descr: 'TrustArc' },
  { selector: '[id*="uniccmp"]', descr: 'UniConsent' },
  { selector: '[class*="sp_message"]', descr: 'Sourcepoint' },
  { selector: '[id*="sp_message"]', descr: 'Sourcepoint' },
  { selector: '[id*="onetrust"]', descr: 'Onetrust' },
  { selector: '[class*="onetrust"]', descr: 'Onetrust' },
  { selector: '[id*="ccpa"]', descr: 'CCPA' },
  { selector: '[class*="ccpa"]', descr: 'CCPA' },
  { selector: '.osano-cm-window', descr: 'osano.com' },
  { selector: '[class*="usercentrics"]', descr: 'usercentrics.com' },
  { selector: '[id*="usercentrics"]', descr: 'usercentrics.com' },
  { selector: '[class*="iubenda"]', descr: 'iubenda.com' },
  { selector: '[id*="iubenda"]', descr: 'iubenda.com' },
  { selector: '[id*="axeptio"]', descr: 'axeptio.eu' },
  { selector: '[class*="axeptio"]', descr: 'iubenda.com' },

  { selector: '[class*="st-cmp"]' },
  { selector: '[class*="eupopup"]' },
  { selector: '.fb-root', descr: 'HNOnline cookie bar' },
  { selector: '.fc-consent-root', descr: 'SME, Aktuality, Dennik N cookies window' },

  /* adblocker */
  { selector: '[id*="ad"][id*="block"]', type: 'adblocker' },
  { selector: '[id*="Ad"][id*="block"]', type: 'adblocker' },
  { selector: '[class*="Ad"][class*="block"]', type: 'adblocker' },
  { selector: '[class*="adblock"]', type: 'adblocker' },
  { selector: '[class*="ad-block"]', type: 'adblocker' },
  { selector: '[class*="ad_block"]', type: 'adblocker' },
  { selector: '#adtoniq-msgr-bar', type: 'adblocker', descr: 'Nano adblocker' },
  { selector: '.fc-ab-root', type: 'adblocker' },
  { selector: '[class*="foxad-"]', type: 'adblocker' },
  { selector: '[class*="unblocker"]', type: 'adblocker' },

  /* push */
  { selector: '[class*="push"][class*="notif"]', type: 'push' },
  { selector: '[id*="push"][id*="notif"]', type: 'push' },
  { selector: '[class*="pushcrew"]', type: 'push', descr: 'VWO Pushcrew' },
  { selector: '[class*="pushly"]', type: 'push', descr: 'VWO Pushcrew' },
  { selector: '[class*="onesignal"]', type: 'push', descr: 'Onesignal - push' },
  { selector: '[class*="fcm"]', type: 'push', descr: 'Firebase Cloud Messaging' },
  { selector: '[id*="fcm"]', type: 'push', descr: 'Firebase Cloud Messaging' },

  /* subscribe */
  { selector: '[id*="newsletter"]', type: 'subscribe' },
  { selector: '[class*="newsletter"]', type: 'subscribe' },
  { selector: 'promo-subscription', type: 'subscribe' },
  { selector: '.promotion-popup', type: 'subscribe' },
  { selector: '[class*="remp-banner"]', type: 'subscribe' },
  { selector: '[id*="remp-banner"]', type: 'subscribe' },

  /* other */
  { selector: '.ad-composer', type: 'other' },
  { selector: '[class*="adthrive"]', type: 'other' },
  { selector: '[class*="customerchat"]', type: 'other' },

]

const WEBSITES = [
  /*
  {
    name: 'bing.com',
    selectors: [
      { selector: '#thp_notf_div', setStyle: [
        { for: "body", style: "visibility: visible;" }
      ]}
    ]
  },
  {    
    name: 'yahoo.com',
    selectors: [
      { selector: '[data-banner-type="ccpa"]', acceptOnly: true },
    ]
  },
  {    
    name: 'producthunt.com',
    selectors: [
      { selector: 'dialog', mustContain: '#ConsentManagerTarget'},
    ]
  }
    selectors: [
      { selector: '[class*="N9d2H"]', mustContain: 'a[class*="y3zKF"]' },
      { selector: '".popup-box.-pro"]', type: 'adblocker' },
    ]
  */
]
