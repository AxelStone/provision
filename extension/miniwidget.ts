const miniwidget = {
  initialised: false,
  data: null as MiniWidgetData,
  observer: null as MutationObserver,
  elMain: null as HTMLElement,
  elDrawer: null as HTMLElement,
  elShowHide: null as HTMLElement,
  elCount: null as HTMLElement,
  elStartStop: null as HTMLElement,
  elIndCookies: null as HTMLElement,
  elIndPush: null as HTMLElement,
  elIndSubscribe: null as HTMLElement,
  elIndAdblocker: null as HTMLElement,
  elIndOther: null as HTMLElement,

  init(data: MiniWidgetData) {
    
    const cssUrl = chrome.runtime.getURL('css/miniwidget.css')
    const htmlUrl = chrome.runtime.getURL('miniwidget.html')

    // insert css and html into document
    return fetch(cssUrl).then((response) => response.text()).then((res: String) => {
      
      res = res.replace(/\{fontRegularUrl\}/g, chrome.runtime.getURL('assets/fonts/SourceSansPro-Regular.ttf'))
      res = res.replace(/\{fontSemiboldUrl\}/g, chrome.runtime.getURL('assets/fonts/SourceSansPro-SemiBold.ttf'))
      document.head.insertAdjacentHTML('beforeend', '<style>' + res + '</style>')
      return fetch(htmlUrl).then((response) => response.text())

    }).then((res) => {

      res = res.replace(/\{iconEye\}/g, chrome.runtime.getURL('assets/img/eye.svg'))
      document.body.insertAdjacentHTML('beforeend', res)
      return Promise.resolve()

    }).then(() => {

      miniwidget.data = data
      miniwidget.elMain = document.body.querySelector('[data-provision="miniwidget"]')
      miniwidget.elDrawer = miniwidget.elMain.querySelector('.pmv-drag')
      miniwidget.elShowHide = miniwidget.elMain.querySelector('.pmv-showall')
      miniwidget.elCount = miniwidget.elMain.querySelector('.pmv-count')
      miniwidget.elStartStop = miniwidget.elMain.querySelector('.pmv-logo')
      miniwidget.elIndCookies = miniwidget.elMain.querySelector('#indicator-c')
      miniwidget.elIndPush = miniwidget.elMain.querySelector('#indicator-p')
      miniwidget.elIndAdblocker = miniwidget.elMain.querySelector('#indicator-a')
      miniwidget.elIndSubscribe = miniwidget.elMain.querySelector('#indicator-s')
      miniwidget.elIndOther = miniwidget.elMain.querySelector('#indicator-o')
      miniwidget.elShowHide.setAttribute('title', chrome.i18n.getMessage("mwShowAll"))
      miniwidget.elStartStop.setAttribute('title', chrome.i18n.getMessage("mwOnOff"))

      // allways append miniwidget to the bottom of body even if another element
      // is additionaly appended to the bottom of body after miniwidget
      miniwidget.observer = new MutationObserver((mutations) => {
        if (miniwidget.elMain.nextSibling) {
          document.body.appendChild(miniwidget.elMain)
        }
      })
      miniwidget.observer.observe(document.body, { childList: true });

      miniwidget.setUIByTabInfo()

      miniwidget.elStartStop.addEventListener('click', ev => {
        chrome.storage.local.get('isOn', function (data) {
          chrome.storage.local.set({ isOn: !data.isOn }, function () {
            !data.isOn ? miniwidget.setStarted() : miniwidget.setStopped()
            !data.isOn ? scanner.start() : scanner.stop()
            !data.isOn ? null : scanner.showFoundElems()
            var message = data.isOn ? { name: 'turn-off' } : { name: 'turn-on' }
            chrome.runtime.sendMessage(message)
          })
        })
      })

      miniwidget.elShowHide.addEventListener('click', ev => {
        let newState = miniwidget.elShowHide.classList.contains('pressed') ? false : true;
        chrome.runtime.sendMessage({
          name: 'show-button-pressed', state: newState
        }, (res) => {
          miniwidget.elShowHide.classList.toggle('pressed', newState)
          newState ? scanner.stop() : scanner.start()
        })
      })

      miniwidget.setPosition(null)

      window.addEventListener("resize", miniwidget.setPosition);

      miniwidget.initDragNDrop()

      miniwidget.initialised = true;
      return Promise.resolve();

    })

  },

  /**
   * used when miniwidget settings change occurs
   */
  show() {
    if (miniwidget.initialised) {
      miniwidget.elMain.classList.toggle('mw-hide', false)
    } else {
      chrome.storage.local.get('miniWidget', (res) => {
        miniwidget.init(res.miniWidget)
      })      
    }
  },

  /**
   * used when miniwidget settings change occurs
   */
  hide() {
    if (miniwidget.initialised) {
      miniwidget.elMain.classList.toggle('mw-hide', true)
    }
  },

  /**
   * Get current tab info and set elements accordingly
   */
  setUIByTabInfo() {
    chrome.runtime.sendMessage({ 
      name: 'get-current-tab-info'
    }, (tabinfo: tabInfo) => { 
      // not if called before the widget html is created
      if (miniwidget.elCount) {
        miniwidget.elShowHide.classList.toggle('pressed', tabinfo.showButtonPressed)
        miniwidget.elCount.innerHTML = (tabinfo.foundElemsCount > 9 ? 9 : tabinfo.foundElemsCount).toString();
        miniwidget.elIndCookies.classList.toggle('active', tabinfo.cookies > 0)
        miniwidget.elIndPush.classList.toggle('active', tabinfo.push > 0)
        miniwidget.elIndSubscribe.classList.toggle('active', tabinfo.subscribe > 0)
        miniwidget.elIndAdblocker.classList.toggle('active', tabinfo.adblocker > 0)
        miniwidget.elIndOther.classList.toggle('active', tabinfo.other > 0)
      }
    })
  },

  initDragNDrop() {
    miniwidget.elDrawer.onmousedown = (event) => {

      const docwidth = document.documentElement.clientWidth
      const elmainDim = miniwidget.elMain.getBoundingClientRect()
      const shiftX = event.clientX - elmainDim.left   
      const moveRange = docwidth - 82
      const centerSnapLeft = (docwidth/2) - 58/2 - 30
      const centerSnapRight = (docwidth/2) - 58/2 + 30
      const centerLeft = (docwidth/2) - 58/2
      let x = null
      let positionX = null

      moveAt(event.pageX)
    
      // moves the widget at (pageX) coordinate
      function moveAt(pageX) {
        x = pageX - shiftX
        // limit left/right side
        if ((x <= moveRange) && (x >= 0)) {
          // snap to center
          if (x >= centerSnapLeft && x <= centerSnapRight) {
            positionX = -1
            miniwidget.elMain.style.left = centerLeft + 'px'
          } else {      
            positionX = x      
            miniwidget.elMain.style.left = x + 'px'
          }
        }
      }    
      function onMouseMove(event) {
        moveAt(event.pageX)
      }    
      // move the widget on mousemove
      document.addEventListener('mousemove', onMouseMove)    
      // drop the widget, remove unneeded handlers
      document.onmouseup = function() {
        document.removeEventListener('mousemove', onMouseMove);
        document.onmouseup = null;
        miniwidget.data.positionX = positionX;
        chrome.storage.local.set({ miniWidget: miniwidget.data })
      }
    
    }

    miniwidget.elDrawer.ondragstart = function() {
      return false;
    };
  },

  setStarted() {
    miniwidget.elMain.classList.toggle('on', true)
    miniwidget.elMain.classList.toggle('off', false)
  },

  setStopped() {
    miniwidget.elMain.classList.toggle('on', false)
    miniwidget.elMain.classList.toggle('off', true)
  },

  setShowFoundElems() {
    miniwidget.elShowHide.classList.toggle('pressed', true)
  },

  setHideFoundElems() {
    miniwidget.elShowHide.classList.toggle('pressed', false)
  },

  onScanCompleted(res: scanCompletedResult) {
    // if not initialised yet (alwaysOn!==true), initialise if there are found elements
    if (!miniwidget.initialised) {
      chrome.storage.local.get('miniWidget', (data) => {
        if (data.miniWidget.isOn && res.foundElemsCount > 0) {
          miniwidget.init(data.miniWidget).then(() => {
            setResult()
          });
        }        
      })      
    } else {
      setResult()
    }

    function setResult() {
      miniwidget.elCount.innerHTML = (res.foundElemsCount > 9 ? 9 : res.foundElemsCount).toString();
      miniwidget.elIndCookies.classList.toggle('active', res.cookies > 0)
      miniwidget.elIndPush.classList.toggle('active', res.push > 0)
      miniwidget.elIndSubscribe.classList.toggle('active', res.subscribe > 0)
      miniwidget.elIndAdblocker.classList.toggle('active', res.adblocker > 0)
      miniwidget.elIndOther.classList.toggle('active', res.other > 0)
    }

  },

  setPosition: (ev) => {
    if (miniwidget.data.positionX !== -1) {
      const docwidth = document.documentElement.clientWidth
      if (miniwidget.data.positionX > docwidth - 82) {
        miniwidget.elMain.style.left = docwidth - 82 + 'px'
      } else if (ev === null) {
        miniwidget.elMain.style.left = miniwidget.data.positionX + 'px'
      }
    }
  }
}