import { tokenToString } from "typescript";
import { Consts, Ruleset } from "./consts";

export const Model = {
  
  url: getBaseUrl(),
  defHeaders: { 
    "Content-Type": "application/json; charset=UTF-8"
  },

  search: (text: string | null = null) => {
    return fetch(Model.url + '/provision/searchrulesets', {
      method: "POST",
      body: JSON.stringify({ secret: Consts.secret , search: text }),
      headers: Model.defHeaders
    }).then(response => response.json()).then(json => json)
    .catch(err => console.log(err));
  },

  login: (email: string, pwd: string) => {

    return fetch(Model.url + '/login', {
      method: "POST",
      body: JSON.stringify({ secret: Consts.secret, email: email, password: pwd, type: Consts.clientType }),
      headers: Model.defHeaders
    }).then(response => response.json()).then(json => json)
    .catch(err => console.log(err));
  },

  googleLogin: (gUser: any) => { 
    
    // let idtoken = gUser.qc !== undefined ? gUser.qc.id_token : gUser.mc.id_token;
    let idtoken = gUser.getAuthResponse().id_token;
    return fetch(Model.url + '/googlelogin', {
      method: "POST",
      body: JSON.stringify({ secret: Consts.secret, id_token: idtoken, type: Consts.clientType }),
      headers: Model.defHeaders
    }).then(response => response.json()).then(json => json)
    .catch(err => console.log(err));
  },

  logout: (token: string) => {

    return fetch(Model.url + '/logout', {
      method: "POST",
      body: JSON.stringify({ secret: Consts.secret, token: token }),
      headers: Model.defHeaders
    }).then(response => response.json()).then(json => json)
    .catch(err => console.log(err));
  },

  register: (email: string, nick: string, pwd: string) => {

    return fetch(Model.url + '/register', {
      method: "POST",
      body: JSON.stringify({ secret: Consts.secret, email: email, nick: nick, password: pwd, type: Consts.clientType }),
      headers: Model.defHeaders
    }).then(response => response.json()).then(json => json)
    .catch(err => console.log(err));
  },

  validateUser: (token: string) => {
    return fetch(Model.url + '/validateuser', {
      method: "POST",
      body: JSON.stringify({ secret: Consts.secret, token: token, type: Consts.clientType }),
      headers: Model.defHeaders
    }).then(response => response.json()).then(json => json)
    .catch(err => console.log(err));
  },

  changePassword: (uid: string, oldpwd: string, newpwd: string) => {
    return fetch(Model.url + '/changepassword', {
      method: "POST",
      body: JSON.stringify({ secret: Consts.secret, uid: uid, oldpwd: oldpwd, newpwd: newpwd }),
      headers: Model.defHeaders
    }).then(response => response.json()).then(json => json)
    .catch(err => console.log(err));
  },
  
  regeneratePassword: (email: string) => {
    return fetch(Model.url + '/regeneratepassword', {
      method: "POST",
      body: JSON.stringify({ secret: Consts.secret, email: email }),
      headers: Model.defHeaders
    }).then(response => response.json()).then(json => json)
    .catch(err => console.log(err));
  },

  validateRulesetName: (name: string, update: string | null = null) => {
    return fetch(Model.url + '/provision/validaterulesetname', {
      method: "POST",
      body: JSON.stringify({ secret: Consts.secret, name: name, update: update }),
      headers: Model.defHeaders
    }).then(response => response.json()).then(json => json)
    .catch(err => console.log(err));
  },

  sendRuleset: (ruleset: any, token: string, update: boolean) => {
    return fetch(Model.url + (ruleset.id !== null ? update ? 
      '/provision/createupdate' : 
      '/provision/editruleset' : 
      '/provision/createnewruleset'
    ), {
      method: "POST",
      body: JSON.stringify({ secret: Consts.secret, token: token, ruleset: ruleset}),
      headers: Model.defHeaders
    }).then(response => response.json()).then(json => json)
    .catch(err => console.log(err));
  },

  getRuleset: (id: string, what: string) => {
    return fetch(Model.url + '/provision/getruleset', {
      method: "POST",
      body: JSON.stringify({ secret: Consts.secret, id: id, what: what }),
      headers: Model.defHeaders
    }).then(response => response.json()).then(json => json)
    .catch(err => console.log(err));
  },

  getUserPosts: (token: string) => {
    return fetch(Model.url + '/provision/getuserposts', {
      method: "POST",
      body: JSON.stringify({ secret: Consts.secret, token: token }),
      headers: Model.defHeaders
    }).then(response => response.json()).then(json => json)
    .catch(err => console.log(err));
  },

  getAdminPosts: (token: string) => {
    return fetch(Model.url + '/provision/getadminposts', {
      method: "POST",
      body: JSON.stringify({ secret: Consts.secret, token: token }),
      headers: Model.defHeaders
    }).then(response => response.json()).then(json => json)
    .catch(err => console.log(err));
  },

  deletePost: (id: string, token: string) => {
    return fetch(Model.url + '/provision/deletepost', {
      method: "POST",
      body: JSON.stringify({ secret: Consts.secret, token: token, id: id }),
      headers: Model.defHeaders
    }).then(response => response.json()).then(json => json)
    .catch(err => console.log(err));
  },

  resolvePost: (res: boolean, ruleset: any, token: string) => {
    return fetch(Model.url + '/provision/adminresolvepost', {
      method: "POST",
      body: JSON.stringify({ secret: Consts.secret, token: token, ruleset: ruleset, result: res }),
      headers: Model.defHeaders
    }).then(response => response.json()).then(json => json)
    .catch(err => console.log(err));
  }
}

function getBaseUrl() {
  let url = '/api';
  switch (process.env.REACT_APP_ENV) {
    case 'prod': url = Consts.productionUrl; break;
    case 'dev':  url = Consts.developmentUrl; break;
  }
  return url;
}