import App from "./App"

export const Consts = {
  version: '0.1.0',
  developmentUrl: 'http://axelproductions86.loc/api',
  productionUrl: 'https://www.axelproductions86.com/api',
  secret: '5d61re5g4r6ef165rh1wrf6d5a',
  clientType: 1 // 1 - editor, 2 - extension 
}

export const DefaultState: State = {
  user: null,
  rulesets: [],
  detail: {
    ruleset: null,
    posts: []
  },
  ui: {
    page: 'login',
    error: null,
    mailsent: false, 
    pwdRegenerated: false,
    pwdChanged: false,
    loading: null,
    validated: false,
    editor: {
      nameExists: false,
      nameErr: null,
      jsonErr: null,
      docErr: null
    }
  }
}

export type CompProps = { app: App };

export type State = {
  user: LoggedUser | null,
  rulesets: Ruleset[],
  detail: {
    ruleset: FullRuleset | FullSuggestion | null,
    posts: Ruleset[],
  }
  ui: {
    page: string | null,
    error: Err | null,
    mailsent: boolean,
    pwdRegenerated: boolean,
    pwdChanged: boolean,
    loading: string | null,
    validated: boolean,
    editor: {
      nameExists: boolean,
      nameErr: string | null,
      jsonErr: string | null,
      docErr: string | null
    }


  },
}

export type Ruleset = {
  id: string,
  name: string,
  rules: string,
  author_id: string,
  status: string,
  adult: string,
  created: string,
  modified: string,
  modified_by_id: string
}

export interface FullRuleset extends Ruleset {
  nickname: string,
  modby_nickname: string,
}

export type Suggestion = {
  id: string,
  ruleset_id: string,
  name: string,
  rules: string,
  author_id: string,
  status: string,
  adult: string,
  created: string,
}

export interface FullSuggestion extends Suggestion {
  nickname: string
}

export type LoggedUser = {
  user: {
    created: string
    email: string
    id: string
    nickname: string
    password: string
    roles: string
    status: string
    type: string
    user_id: string | null
  },
  token: string,
  posts: Suggestion[]
}

export type Err = {
  code: string,
  text: string
}

export const ErrorCodes = {
  SOMETHING_WENT_WRONG: '0',
  WRONG_USERNAME_PASSOWRD: '1',
  USER_EXISTS: '2',
  ACCOUNT_NOT_ACTIVATED: '3',
  WRONG_PASSOWRD: '4',
  WRONG_EMAIL: '5',
  RULESET_NAME_EXISTS: '6',
  RULESET_INVALID_NAME: '7',
  RULESET_INVALID_JSON: '8',
  RULESET_DOCUMENTATION_NOT_READED: '9',
  ACCESS_DENIED: '10',
}

export const RuleTypes = ['cookies', 'subscribe', 'adblocker', 'push', 'other'];