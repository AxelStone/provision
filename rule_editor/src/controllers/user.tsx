import App from "../App";
import { Ruleset } from "../consts";
import { Model } from "../model";

export function getUserController(app: App) {
  return {
    
    APIGoogleLogin: (gUser: any) => { console.log('login', gUser)
      Model.googleLogin(gUser).then(res => { 
        if (res) {
          res.posts = [];
          app.setState({ user: res })
          app.funcs.common.switchPage('profile');
          // set cookie
          let date = new Date();
          date.setTime(date.getTime()+(30*24*60*60*1000));
          let expires = date.toUTCString();
          document.cookie = "token=" + res.token + "; expires=" + expires + ";";
        } else {
          app.funcs.user.APILogout();
        }
      })
    },

    APILogin: () => {
      let ui = app.state.ui; 
      let email = (document.getElementById('email') as HTMLInputElement).value;
      let pwd = (document.getElementById('pwd') as HTMLInputElement).value;

      if (app.funcs.user.validateField('email') && app.funcs.user.validateField('pwd')) {
        ui.loading = 'login';
        app.setState({ ui: ui });
        Model.login(email, pwd).then(res => {
          if (res.error) {
            ui.loading = null;
            ui.error = res.error;
            app.setState({ ui: ui });
          } else {
            app.setState({ user: { user: res.user, token: res.token, posts: [] }})
            app.funcs.common.switchPage('profile');
            // set cookie
            let date = new Date();
            date.setTime(date.getTime()+(30*24*60*60*1000));
            let expires = date.toUTCString();
            document.cookie = "token=" + res.token + "; expires=" + expires + ";";
          }
        })
      }
      
    },

    APIRegister: () => {
      let ui = app.state.ui;
      let email = (document.getElementById('email') as HTMLInputElement).value;
      let nick = (document.getElementById('nick') as HTMLInputElement).value;
      let pwd1 = (document.getElementById('pwd1') as HTMLInputElement).value;

      let err1 = !app.funcs.user.validateRegistrationField('blur', 'email') 
      let err2 = !app.funcs.user.validateRegistrationField('blur', 'nick')
      let err3 = !app.funcs.user.validateRegistrationField('blur', 'pwd1')
      let err4 = !app.funcs.user.validateRegistrationField('blur', 'pwd2')

      if (!err1 && !err2 && !err3 && !err4) {
        ui.loading = 'register';
        Model.register(email, nick, pwd1).then(res => {
          if (res.error) {
            ui.loading = null;
            ui.error = res.error;
            app.setState({ ui: ui })
          } else {
            app.funcs.common.switchPage('login').then(() => {
              ui.mailsent = true;
              app.setState({ ui: ui });
            });
          }
        })
        return true;
      } else {
        return false;
      }

    },

    APILogout: () => {
      let ui = app.state.ui;
      if (app.state.user) {
        document.cookie = "token=; expires=Thu, 01 Jan 1970 00:00:00 UTC;";
        ui.loading = 'logout'
        app.setState({ ui: ui })
        Model.logout(app.state.user.token).then(res =>{
          var auth2 = (window as any).gapi.auth2.getAuthInstance();
          auth2.signOut().then((res: any) =>{
            window.location.reload();
          })
        });
      }
    },

    APIValidateUser: (redirect: boolean = true) => {
      let token = app.funcs.user.getCookieToken();        
      if (token) {
        let ui = app.state.ui;
        ui.loading = 'validateuser';
        app.setState({ ui: ui })
        Model.validateUser(token).then(user => {
          // remove cookie if token is invalid
          if (!user) {
            document.cookie = "token=; expires=Thu, 01 Jan 1970 00:00:00 UTC;";
            app.setState({ user: null })
            app.funcs.user.APILogout();
            app.funcs.common.switchPage('login')
          } else {
            app.setState({ user: { user: user, token: token, posts: [] }}, () => {
              app.funcs.common.switchPage('profile');
            })
          }
        })
      } else {
        app.funcs.common.switchPage('login');
      }
    },

    APIChangePwd: () => {
      let ui = app.state.ui;
      let oldpwd = (document.getElementById('old-pwd') as HTMLInputElement);
      let newpwd1 = (document.getElementById('new-pwd1') as HTMLInputElement);
      let newpwd2 = (document.getElementById('new-pwd2') as HTMLInputElement);

      let err1 = !app.funcs.user.validateChangePwdField('blur', 'old-pwd')
      let err2 = !app.funcs.user.validateChangePwdField('blur', 'new-pwd1')
      let err3 = !app.funcs.user.validateChangePwdField('blur', 'new-pwd2')

      if (!err1 && !err2 && !err3 && app.state.user) {
        ui.loading = 'changepwd';
        app.setState({ ui: ui })
        Model.changePassword(app.state.user?.user.id, oldpwd.value, newpwd1.value).then(res => {
          if (res.error) {
            ui.loading = null;
            ui.error = res.error;
            app.setState({ ui: ui })
          } else {
            app.funcs.user.APILogout();
          }
        })
      }

    },

    APIPasswordRecovery: () => {
      let ui = app.state.ui;
      let email = (document.getElementById('email') as HTMLInputElement).value.trim();
      if (email.length < 8 || email.indexOf('@') < 2 || email.indexOf('.') < 1 || email.indexOf(' ') > -1 ||
          email.lastIndexOf('.') < (email.lastIndexOf('@') + 2) || (email.length - email.lastIndexOf('.')) < 3
      ) {
        let ui = app.state.ui;
        ui.error = { code: '5', text: 'Wrong email' };
        app.setState({ ui: ui })
      } else {
        ui.error = null;
        ui.loading = 'pwdrecovery';
        app.setState({ ui: ui });
        Model.regeneratePassword(email).then(res => {
          if (res.error) {
            ui.loading = null;
            ui.error = res.error;
            app.setState({ ui: ui });
          } else {
            ui.loading = null;
            ui.pwdRegenerated = true;
            ui.error = null;
            app.setState({ ui: ui });
            email = '';
          }
        })
      }
    },

    validateField: (what:string) => { 
      let email = (document.getElementById('email') as HTMLInputElement).value.trim()
      let pwd = (document.getElementById('pwd') as HTMLInputElement).value;
      let isErr = false;

      if (what === 'email') {
        if (email.length < 8 || email.indexOf('@') < 2 || email.indexOf('.') < 1 || email.indexOf(' ') > -1 ||
            email.lastIndexOf('.') < (email.lastIndexOf('@') + 2) || (email.length - email.lastIndexOf('.')) < 3
        ) {
          isErr = true;
        }
      }

      if (what === 'pwd') {
        if (pwd.length < 6) {
          isErr = true;
        }
      }

      let ui = app.state.ui;
      ui.error = isErr ? { code: '1', text: 'Wrong email or password' } : null;
      app.setState({ ui: ui })
      return !isErr;
    },

    validateRegistrationField: (type: string, what:string) => { 
      let email = (document.getElementById('email') as HTMLInputElement).value.trim()
      let nick = (document.getElementById('nick') as HTMLInputElement).value.trim();
      let pwd1 = (document.getElementById('pwd1') as HTMLInputElement).value;
      let pwd2 = (document.getElementById('pwd2') as HTMLInputElement).value;
      let emailicon = (document.getElementById('emailicon') as HTMLElement);
      let nickicon = (document.getElementById('nickicon') as HTMLElement);
      let pwd1icon = (document.getElementById('pwd1icon') as HTMLElement);
      let pwd2icon = (document.getElementById('pwd2icon') as HTMLElement);
      let isErr = false;

      if (what === 'email') {
        if (email.length < 8 || email.indexOf('@') < 2 || email.indexOf('.') < 1 || email.indexOf(' ') > -1 ||
            email.lastIndexOf('.') < (email.lastIndexOf('@') + 2) || (email.length - email.lastIndexOf('.')) < 3
        ) {
          emailicon.classList.add(type === 'change' ? 'notok' : 'err')
          emailicon.classList.remove('ok')
          emailicon.classList.remove(type === 'change' ? 'err' : 'notok')
          isErr = true;
        } else {
          emailicon.classList.add('ok')
          emailicon.classList.remove('notok')
          emailicon.classList.remove('err')
        }
      }

      if (what === 'nick') {
        if (nick.length < 5) {
          nickicon.classList.add(type === 'change' ? 'notok' : 'err')
          nickicon.classList.remove('ok')
          nickicon.classList.remove(type === 'change' ? 'err' : 'notok')
          isErr = true;
        } else { 
          nickicon.classList.add('ok')
          nickicon.classList.remove('notok')
          nickicon.classList.remove('err')
        }
      }

      if (what === 'pwd1') {
        if (pwd1.length < 6) {
          pwd1icon.classList.add(type === 'change' ? 'notok' : 'err')
          pwd1icon.classList.remove('ok')
          pwd1icon.classList.remove(type === 'change' ? 'err' : 'notok')
          isErr = true;
        } else {
          pwd1icon.classList.add('ok')
          pwd1icon.classList.remove('notok')
          pwd1icon.classList.remove('err')
        }
      }

      if (what === 'pwd2') {
        if (pwd1 !== pwd2) {
          pwd2icon.classList.add(type === 'change' ? 'notok' : 'err')
          pwd2icon.classList.remove('ok')
          pwd2icon.classList.remove(type === 'change' ? 'err' : 'notok')
          isErr = true;
        } else {
          pwd2icon.classList.add('ok')
          pwd2icon.classList.remove('notok')
          pwd2icon.classList.remove('err')
        }
      }

      let ui = app.state.ui;
      ui.error = isErr ? { code: '0', text: 'Not valid data' } : null;
      app.setState({ ui: ui })
      return !isErr;
    },

    validateChangePwdField: (type: string, what:string) => {
      let oldpwd = (document.getElementById('old-pwd') as HTMLInputElement).value;
      let newpwd1 = (document.getElementById('new-pwd1') as HTMLInputElement).value;
      let newpwd2 = (document.getElementById('new-pwd2') as HTMLInputElement).value;
      let oldicon = (document.getElementById('oldpwdicon') as HTMLElement);
      let pwd1icon = (document.getElementById('newpwd1icon') as HTMLElement);
      let pwd2icon = (document.getElementById('newpwd2icon') as HTMLElement);
      let isErr = false;

      if (what === 'old-pwd') {
        if (oldpwd.length < 6) {
          oldicon.classList.add(type === 'change' ? 'notok' : 'err')
          oldicon.classList.remove('ok')
          oldicon.classList.remove(type === 'change' ? 'err' : 'notok')
          isErr = true;
        } else {
          oldicon.classList.add('ok')
          oldicon.classList.remove('notok')
          oldicon.classList.remove('err')
        }
      }

      if (what === 'new-pwd1') {
        if (newpwd1.length < 6) {
          pwd1icon.classList.add(type === 'change' ? 'notok' : 'err')
          pwd1icon.classList.remove('ok')
          pwd1icon.classList.remove(type === 'change' ? 'err' : 'notok')
          isErr = true;
        } else {
          pwd1icon.classList.add('ok')
          pwd1icon.classList.remove('notok')
          pwd1icon.classList.remove('err')
        }
      }

      if (what === 'new-pwd2') {
        if (newpwd1 !== newpwd2) {
          pwd2icon.classList.add(type === 'change' ? 'notok' : 'err')
          pwd2icon.classList.remove('ok')
          pwd2icon.classList.remove(type === 'change' ? 'err' : 'notok')
          isErr = true;
        } else {
          pwd2icon.classList.add('ok')
          pwd2icon.classList.remove('notok')
          pwd2icon.classList.remove('err')
        }
      }

      let ui = app.state.ui;
      ui.error = isErr ? { code: '0', text: 'Not valid data' } : null;
      app.setState({ ui: ui })
      return !isErr;
    },

    getCookieToken: () => {
      let cookies = document.cookie.split('; ');
      let token = null;
      for(let i = 0; i < cookies.length; i++) {
        if (cookies[i].indexOf('token=') > -1) {
          token = cookies[i].replace('token=', '').trim();  
        }
      } 
      return token;
    },

    hasRights: (forWhat: string) => {

      let access = false;
      switch (forWhat) {
        case 'create-post': 
          access = app.state.user !== null && (app.state.user?.user.roles.indexOf('0') > -1 || app.state.user?.user.roles.indexOf('1') > -1) 
          break;
      }
      return access;
    }

  }
}