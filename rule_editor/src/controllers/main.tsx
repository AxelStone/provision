import App from "../App";
import { ErrorCodes, FullRuleset, LoggedUser, RuleTypes } from "../consts";
import { Helper } from "../helper";
import { Model } from "../model";

export function getMainController(app: App) {
  return {
    APISendRuleset: () => {
      let nameEl = document.getElementById('ruleset-name') as HTMLInputElement;
      let name = parseName(nameEl.value);
      let jsonEl = document.getElementById('ruleset-json') as HTMLInputElement;
      let json = parseJson(jsonEl.value);
      let notMatEl = document.getElementById('ruleset-mature-no') as HTMLInputElement; 
      let mature = !notMatEl.checked;
      let ui = app.state.ui;
      ui.loading = 'ruleset-send';
      app.setState({ ui: ui })
      let id = (ui.page === 'edit-post' || ui.page === 'create-update' ? app.state.detail.ruleset?.id : null) as string | null;
      let ruleset = { name: name, json: JSON.stringify(json), mature: mature, id: id }
      let update = ui.page === 'create-update';
      Model.sendRuleset(
        ruleset, app.state.user?.token as string, update
      ).then(res => {
        if (res.error) {
          ui.error = res.error;
          ui.validated = false;
          ui.editor.nameExists = true;
          ui.loading = null;
          app.setState({ ui: ui })
        } else {
          app.funcs.common.search();
          app.funcs.common.switchPage('profile');
        }
      })
    },

    APIGetProfilePosts: () => {
      let ui = app.state.ui;
      let user = app.state.user as LoggedUser;
      ui.loading = 'profile-posts';
      let apicall = () => {
        if (user.user.roles === '0') {
          return Model.getAdminPosts(user.token);
        } else {
          return Model.getUserPosts(user.token)
        }
      }

      app.setState({ ui: ui }, () => { 
        apicall().then(res => {
          if (res.error) {
            ui.error = res.error;
            ui.loading = null;
            app.setState({ ui: ui })
          } else { 
            (user as LoggedUser).posts = res.posts;
            ui.loading = null;
            app.setState({ ui: ui, user: user }) 
          }
        })
      })
    },

    APIDeletePost: (id: string) => {
      let ui = app.state.ui;
      let user = app.state.user as LoggedUser;
      ui.loading = 'post-delete-' + id;
      app.setState({ ui: ui });
      Model.deletePost(id, user.token).then(res => {
        if (res.error) {
          ui.error = res.error;
          ui.loading = null;
          app.setState({ ui: ui })
        } else { 
          user.posts = res.posts;
          ui.loading = null;
          app.setState({ ui: ui, user: user }, () => {}) 
        }
      })
    },

    APIResolveRuleset: (res: boolean) => {
      let nameEl = document.getElementById('ruleset-name') as HTMLInputElement;
      let name = parseName(nameEl.value);
      let jsonEl = document.getElementById('ruleset-json') as HTMLInputElement;
      let json = parseJson(jsonEl.value);
      let notMatEl = document.getElementById('ruleset-mature-no') as HTMLInputElement; 
      let mature = !notMatEl.checked;
      let ui = app.state.ui;
      let user = app.state.user as LoggedUser;
      let id = app.state.detail.ruleset?.id as string;
      ui.loading = 'post-resolve';
      app.setState({ ui: ui });
      let ruleset = { name: name, json: JSON.stringify(json), mature: mature, id: id }
      Model.resolvePost(res, ruleset, app.state.user?.token as string).then(res => {
        if (res.error) {
          ui.error = res.error;
          ui.loading = null;
          app.setState({ ui: ui })
        } else {
          app.funcs.common.search(); 
          app.funcs.common.switchPage('profile')
        }
      })  
    },

    validateAll: (online: boolean = false) => {
      setTimeout(() => {
        let ui = app.state.ui;
        app.setState({ ui: ui }, () => {
          app.funcs.main.validate('doc').then(() => 
            app.funcs.main.validate('json').then(() =>
              app.funcs.main.validate('name', false).then(() => {
                let ui = app.state.ui;
                ui.validated = true;
                app.setState({ ui: ui })
              })
            )
          )
        });
      }, 10)
    },

    validate: (what: string, online: boolean = false) => { console.log('validate', what + ' ' + online);

      let ui = app.state.ui;
      let detail = app.state.detail;

      return new Promise<void>((resolve) => {
        switch (what) {

          case 'name': 
            let nameEl = document.getElementById('ruleset-name') as HTMLInputElement;
            let name = parseName(nameEl.value);
            nameEl.value = name;
            // detail.posts = [];
            app.setState({ detail: detail })
            if (name.length < 3) {            
              ui.editor.nameErr = 'Invalid name';
              app.setState({ ui: ui }, () => resolve())
            } else {
              ui.editor.nameErr = null;
              ui.loading = online ? 'ruleset-name' : null;
              app.setState({ ui: ui }, () => { 
                if (online) {
                  let isUpdate = detail.ruleset !== null && (detail.ruleset as FullRuleset).modified !== undefined;
                  console.log('upd', isUpdate)
                  Model.validateRulesetName(name, isUpdate ? detail.ruleset?.name : null).then(res => {
                    if (res.error) { 
                      ui.editor.nameExists = true;
                    } else {
                      ui.editor.nameExists = false;
                    }
                    ui.loading = null;
                    detail.posts = res.posts;
                    app.setState({ ui: ui, detail: detail }, () => {
                      // app.funcs.main.validateAll(false);
                    })
                  })
                }
                resolve() 
              }); 
            }
            break;
  
          case 'json':
  
            let jsonEl = document.getElementById('ruleset-json') as HTMLInputElement;
            let json = parseJson(jsonEl.value);
            let error = Helper.validateRulesetJson(json);
            if (error !== null) { ui.editor.jsonErr = error; } else { ui.editor.jsonErr = null} 
            app.setState({ ui: ui }, () => resolve())
            break;   
            
          case 'doc':

            let docEl = document.getElementById('ruleset-documentation') as HTMLInputElement;
            if (docEl !== null && !docEl.checked) {
              ui.editor.docErr = 'Please read the documentation first!' 
            } else {
              ui.validated = false;
              ui.editor.docErr = null;
            }
            app.setState({ ui: ui }, () => resolve())            
            break
        }

      })

    },

    setMature: (m: boolean) => {
      let notMatEl = document.getElementById('ruleset-mature-no') as HTMLInputElement; 
      let matEl = document.getElementById('ruleset-mature-yes') as HTMLInputElement; 
      notMatEl.checked = m ? false : true;
      matEl.checked = m ? true : false;
    },

    setValid: (v: boolean) => {
      let ui = app.state.ui;
      ui.validated = v;
      app.setState({ui: ui});
    },

    setNameExists: (n: boolean) => {
      let ui = app.state.ui;
      ui.editor.nameExists = n;
      app.setState({ui: ui});
    }
  }
}

function parseJson(s: string) {
  try {
    var o = JSON.parse(s);
    if (o && typeof o === "object") { return o; }
  } catch (e) { }
  return false
}

function parseName(n: string) {
  n.replaceAll('http://', '').replaceAll('https://', '').replaceAll('www.', '');
  n = n.split('/')[0].split('?')[0].split('#')[0].split(':')[0];
  return n;
}