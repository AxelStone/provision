import App from "../App";
import { FullRuleset, Ruleset, Suggestion } from "../consts";
import { Model } from "../model";

export function getCommonController(app: App) {
  return {
    
    switchPage: (page: string) => {
      (window as any).gButtonRendered = false;
      let ui = app.state.ui;
      let detail = app.state.detail;
      detail.posts = [];
      ui.page = page;
      ui.error = null;
      ui.mailsent = false;
      ui.pwdRegenerated = false;
      ui.pwdChanged = false;
      ui.loading = null;
      ui.validated = false;
      ui.editor.nameExists = false;
      const url = new URL(window.location.toString());
      url.searchParams.delete('page');
      window.history.replaceState(null, '', url.toString())

      return new Promise<void>((resolve) => {
        app.setState({ ui: ui, detail: detail }, () => {
          resolve();
        });
      })

    },

    search: () => { 
      let text = (document.getElementById('search-input') as HTMLInputElement).value;
      let ui = app.state.ui;
      let txt: any = null;
      txt = (text == null || text.length < 3) ? null : text;
      ui.loading = 'search';
      app.setState({ ui: ui }); 
      Model.search(txt).then(res => { 
        let ui = app.state.ui;
        ui.loading = null;
        app.setState({ ui: ui, rulesets: res ? res : [] })
      });
    },

    openRuleset: (ruleset: Ruleset | Suggestion, edit: boolean = false) => {
      let detail = app.state.detail;
      let ui = app.state.ui;
      ui.loading = 'ruleset';
      app.setState({ ui: ui });
      Model.getRuleset(ruleset.id, (ruleset as Ruleset).modified !== undefined ? 'ruleset' : 'post').then(res => {
        if (res.error) {} else {
          if (edit) {
            detail.ruleset = res; 
            app.setState({ detail: detail });
            app.funcs.common.switchPage('edit-post').then(() => {
              (document.getElementById('ruleset-name') as HTMLInputElement).value = ruleset.name;
              (document.getElementById('ruleset-json') as HTMLTextAreaElement).value = JSON.stringify(JSON.parse(ruleset.rules), null, 2);
              (document.getElementById('ruleset-mature-no') as HTMLInputElement).checked = ruleset.adult === '0';
              (document.getElementById('ruleset-mature-yes') as HTMLInputElement).checked = ruleset.adult === '1';
            });
          } else { 
            detail.ruleset = res;    
            console.log('detail', detail)  
            app.setState({ detail: detail }, () => { 
              app.funcs.common.switchPage('open-ruleset');
            });
          }
        }
      })      
    },

    openSuggestUpdate: () => {
      let ruleset = app.state.detail.ruleset as FullRuleset;
      app.funcs.common.switchPage('create-update').then(() => {
        (document.getElementById('ruleset-name') as HTMLInputElement).value = ruleset.name;
        (document.getElementById('ruleset-json') as HTMLTextAreaElement).value = JSON.stringify(JSON.parse(ruleset.rules), null, 2);
        (document.getElementById('ruleset-mature-no') as HTMLInputElement).checked = ruleset.adult === '0';
        (document.getElementById('ruleset-mature-yes') as HTMLInputElement).checked = ruleset.adult === '1';
      });
    },

    openPwdManager: () => {
      document.getElementById('pass-manager')?.classList.toggle('opened');
    }

  }
}