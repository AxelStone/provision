import App from "../App";
import { Ruleset } from "../consts";
import { Model } from "../model";
import { getCommonController } from "./common";
import { getMainController } from "./main";
import { getUserController } from "./user";


export function getController(app: App) {
  return {

    common: getCommonController(app),
    main: getMainController(app),
    user: getUserController(app),

  }
}