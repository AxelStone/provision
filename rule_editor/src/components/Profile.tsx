import React, { useEffect } from "react";
import { CompProps, ErrorCodes, Suggestion } from "../consts";
import { Model } from "../model";
import DeleteButton from "./common/DeleteButton"; 

export const Profile: React.FC<CompProps> = (props: CompProps) => {
  
  useEffect(() => { props.app.funcs.main.APIGetProfilePosts() }, [])

  return (
    <div className="profilecard">
      <h2>
        <i className="fas fa-user-circle"></i>USER PROFILE
      </h2>

      <div className="flex">

        <div className="basic-info">
          <div className="field name">
            <div className="value">{props.app.state.user?.user.nickname}</div>
          </div>
          <div className="field email">
            <div className="value">{props.app.state.user?.user.email}</div>
          </div>
          <div className="field account">
            <div className="value">
              {props.app.state.user?.user.type === '1' ? 'Local account' : 'Google account'},
              &nbsp;{(new Date(parseInt(props.app.state.user?.user.created as string)*1000)).toISOString().substr(0,10)}             
            </div>
          </div>
        </div>

        { props.app.state.user?.user.type === '1' ? (
          <div className="change-password" id="pass-manager" >

            <button className="field" onClick={() => { props.app.funcs.common.openPwdManager() }}>
              <div className="label">Password management <i className="fas fa-chevron-down"></i></div>
            </button>

            <div className="cont">
              { props.app.state.ui.pwdChanged ? <div className="message">
                  <i className="fas fa-check"></i> Password has been changed!
                </div> : ''
              }

              <input type="password" id="old-pwd" placeholder="Current password" 
                onChange={() => props.app.funcs.user.validateChangePwdField('change', 'old-pwd')}
                onBlur={() => props.app.funcs.user.validateChangePwdField('change', 'old-pwd')}
              /><span id="oldpwdicon" className="inputicon"><i className="fas fa-check"></i><i className="fas fa-times"></i></span>
              { (props.app.state.ui.error && props.app.state.ui.error.code == ErrorCodes.WRONG_PASSOWRD) ? 
              <div className="error">Wrong password!</div> : null }
              
              <input type="password" id="new-pwd1" placeholder="New password" 
                onChange={() => props.app.funcs.user.validateChangePwdField('change', 'new-pwd1')}
                onBlur={() => props.app.funcs.user.validateChangePwdField('change', 'new-pwd1')}
              /><span id="newpwd1icon" className="inputicon"><i className="fas fa-check"></i><i className="fas fa-times"></i></span>

              <input type="password" id="new-pwd2" placeholder="Repeat new password" 
                onChange={() => props.app.funcs.user.validateChangePwdField('change', 'new-pwd2')}
                onBlur={() => props.app.funcs.user.validateChangePwdField('change', 'new-pwd2')}
              /><span id="newpwd2icon" className="inputicon"><i className="fas fa-check"></i><i className="fas fa-times"></i></span>

              <div className="wrap">
                <button className="button" id="change-pwd" onClick={() => props.app.funcs.user.APIChangePwd() }>
                { props.app.state.ui.loading !== 'changepwd' ? <i className="fas fa-key"></i> : <i className="fas fa-spinner fa-spin"></i> }
                  Change
                </button>
              </div>
            </div>

          </div>
        ) : ''}

      </div>

      <div className="posts">
        <div className="field"><span className="label">Your rulesets</span></div>
        { props.app.state.user?.posts.map((post, ind) => 
            post.status !== '2' ? <PostRow post={post} loading={props.app.state.ui.loading} funcs={props.app.funcs}/> : ''
        )} 
        { props.app.state.user?.posts.map((post, ind) => 
            post.status === '2' ? <PostRow post={post} loading={props.app.state.ui.loading} funcs={props.app.funcs}/> : ''
        )} 
        { props.app.state.user?.posts.length === 0 ? <div>You don't have any rulesets yet.</div> : '' }
      </div>

    </div>
  );
}

export const PostRow: React.FC<{ 
  post: Suggestion, loading: string | null, funcs: any
}> = (props: { 
  post: Suggestion, loading: string | null, funcs: any
}) => {

  return (
    <div className={"item" + (props.post.status === '2' ? ' rejected' : '')}>
      <div className="status">{
        props.post.status === '0' ? <span><i className="fas fa-hourglass-half"></i></span> :
        props.post.status === '1' ? <span><i className="fas fa-check"></i></span> :
        props.post.status === '2' ? <span><i className="fas fa-times"></i></span> : ''
      }</div>
      <div className="type">{ 
        props.post.ruleset_id !== null ? 
          <span><i className="fas fa-edit"></i></span> :
          <span><i className="fas fa-folder-plus"></i></span>
      }</div>
      <div className="name">{ props.post.name }</div>
      <div className="row-actions">
        { props.post.status === '0' ? 
          <DeleteButton id={props.post.id} for="post" loading={props.loading} onDelete={props.funcs.main.APIDeletePost} /> : ''}
        { props.post.status === '0' ? 
          <button className="edit" onClick={() => { props.funcs.common.openRuleset(props.post, true) }}>
            <i className="fas fa-pencil-alt"></i>
          </button> :
          <button className="view" onClick={() => { props.funcs.common.openRuleset(props.post) }}>
            <i className="far fa-eye"></i>
          </button> 
        }
      </div>
    </div>
  );
}