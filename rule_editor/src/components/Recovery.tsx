import React from "react";
import { CompProps, ErrorCodes } from "../consts";

export const Recovery: React.FC<CompProps> = (props: CompProps) => {

  return (
    <div className="recoverycard">

      <h2><i className="fas fa-user-circle"></i>PASSWORD RECOVERY</h2>

      { props.app.state.ui.pwdRegenerated ? <div className="message">
          <i className="far fa-envelope"></i> An e-mail containing your new password has been sent to your mailbox!
        </div> : ''
      }

      <div className="form">
        <div className="widthwrap">  

          <input id="email" placeholder="e-mail"/>
          { (props.app.state.ui.error && props.app.state.ui.error.code == ErrorCodes.WRONG_EMAIL) ? 
          <div className="error">Wrong e-mail!</div> : null }    

          <div className="wrap">
            <div className="left">
              <button id="register-new" onClick={() => props.app.funcs.common.switchPage('login') }>
                Login as existing user
              </button><br />
            </div>
            <div className="right">
              <button className="button" id="send-recovery" onClick={() => props.app.funcs.user.APIPasswordRecovery() }>
              { props.app.state.ui.loading !== 'pwdrecovery' ? <i className="fas fa-paper-plane"></i> : <i className="fas fa-spinner fa-spin"></i> }
              Send
            </button>
            </div>
          </div>
        </div>

      </div>

      <div className="text">   
        <div className="widthwrap">       
          Your new password will be sent to your email.
        </div>
      </div>  

    </div>
  );
}

