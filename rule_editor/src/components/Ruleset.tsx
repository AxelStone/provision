import React from "react";
import { CompProps, FullRuleset, Ruleset as RS, Suggestion } from "../consts";

export const Ruleset: React.FC<CompProps> = (props: CompProps) => {

  return (
    props.app.state.detail.ruleset ? <div className="rulesetcard">
      
      <h2><i className="fas fa-globe-americas"></i>{props.app.state.detail.ruleset.name}</h2>

      <div className="field ruleset">
        <div className="value sc"><pre>
          { JSON.stringify(JSON.parse(props.app.state.detail.ruleset.rules), null, 2) }
        </pre></div>
      </div>
      <div className="field info">

          {(props.app.state.detail.ruleset as FullRuleset).modified ? 
            <div>
              Last modified by {(props.app.state.detail.ruleset as FullRuleset).modby_nickname} 
              &nbsp;on {(new Date(parseInt((props.app.state.detail.ruleset as FullRuleset).modified + '000'))).toISOString().substr(0, 10)}
            </div> : <div>
              Created by { props.app.state.detail.ruleset.author_id === '0' ? 'Axel' : props.app.state.detail.ruleset.nickname }
              &nbsp;on {(new Date(parseInt(props.app.state.detail.ruleset.created + '000'))).toISOString().substr(0, 10)} 
            </div> 
          }
      </div>

      <div className="additional">
        { (props.app.state.detail.ruleset as FullRuleset).modified === undefined ?
          <div className="field status">
            <span className="label">status:</span>
            <span className="value">{
              props.app.state.detail.ruleset.status === '0' ? <span><i className="fas fa-hourglass-half"></i>Waiting</span> : 
              props.app.state.detail.ruleset.status === '1' ? <span><i className="fas fa-check"></i>Approved</span> :
              props.app.state.detail.ruleset.status === '2' ? <span><i className="fas fa-times"></i>Rejected</span> : ''
            }</span>
          </div> : '' }
      </div>

    </div> : <div></div>
  );
}


