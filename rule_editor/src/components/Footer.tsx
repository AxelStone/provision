import React from "react";
import { CompProps } from "../consts";

export const Footer: React.FC<CompProps> = (props: CompProps) => {

  return (
    <footer>
      <div>
        <span className="copyright">&copy; 2021 All rights reserved.</span>
        <span className="creator">With <i className="fas fa-heart"></i> created by Team Axel</span>
      </div>
    </footer>
  );
}


