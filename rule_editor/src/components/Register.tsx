import React, { useEffect, useState } from "react";
import { CompProps, ErrorCodes } from "../consts";

export const Register: React.FC<CompProps> = (props: CompProps) => {

  useEffect(() => {
    setTimeout(() => {
      if ((window as any).gapi && !(window as any).gButtonRendered && 
        document.querySelector('#g-signin2') && !document.querySelector('#g-signin2 > div')
      ) { 
        (window as any).gButtonRendered = true;
        (window as any).gapi.signin2.render('g-signin2', {
          'scope': 'profile email', 'width': 262, 'height': 38, 'longtitle': true, 'theme': 'light',
          'onsuccess': (googleUser: any) => { document.dispatchEvent(new CustomEvent('google-signin', { detail: { googleUser }})) },
          'onfailure': (err: any) => { console.log('google err', err) }
        });
      }
    }, 100);
  })

  return (
    <div className="registercard">

      <h2>
        <div className="widthwrap"><i className="fas fa-user-circle"></i>REGISTER NEW ACCOUNT</div>
      </h2>

      <div className="form">
        <div className="widthwrap">

          <div id="g-signin2"></div>
          
          {/*<div className="info">Your account name may be visible to other users</div>*/}
          <div className="divider">
            <span>OR</span>
          </div>

          <input id="email" placeholder="E-mail" 
            onChange={(e) => props.app.funcs.user.validateRegistrationField('change', 'email') } 
            onBlur={(e) => props.app.funcs.user.validateRegistrationField('blur', 'email') }
          /><span id="emailicon" className="inputicon">
            <i className="fas fa-check"></i><i className="fas fa-times"></i>
          </span>
          { (props.app.state.ui.error && props.app.state.ui.error.code == ErrorCodes.USER_EXISTS) ? 
          <div className="error">User with this email already exists!</div> : null }
          <input id="nick" placeholder="Nickname" 
            onChange={(e) => props.app.funcs.user.validateRegistrationField('change', 'nick') } 
            onBlur={(e) => props.app.funcs.user.validateRegistrationField('blur', 'nick') }
          /><span id="nickicon" className="inputicon">
            <i className="fas fa-check"></i><i className="fas fa-times"></i>
          </span><div className="info">Your nickname may be visible to other users</div>
          <input id="pwd1" placeholder="Password" type="password" 
            onChange={(e) => props.app.funcs.user.validateRegistrationField('change', 'pwd1') } 
            onBlur={(e) => props.app.funcs.user.validateRegistrationField('blur', 'pwd1') }
          /><span id="pwd1icon" className="inputicon">
            <i className="fas fa-check"></i><i className="fas fa-times"></i>
          </span>
          <input id="pwd2" placeholder="Repeat password" type="password" 
            onChange={(e) => props.app.funcs.user.validateRegistrationField('change', 'pwd2') } 
            onBlur={(e) => props.app.funcs.user.validateRegistrationField('blur', 'pwd2') }
          /><span id="pwd2icon" className="inputicon">
            <i className="fas fa-check"></i><i className="fas fa-times"></i>
          </span>

          <div className="wrap">
            <div className="left">
              <button id="login-existing" onClick={() => props.app.funcs.common.switchPage('login') }>
                Login as existing user
              </button>
            </div>
            <div className="right">
              <button className="button" id="register" onClick={() => props.app.funcs.user.APIRegister() }>
                { props.app.state.ui.loading !== 'register' ? <i className="fas fa-paper-plane"></i> : <i className="fas fa-spinner fa-spin"></i> }
                Register
              </button>
            </div>
          </div>
          
        </div>
      </div>

      <div className="text">          
        <div className="widthwrap">
          We don't send newsletters nor any other promotional material. 
          All the emails you will recieve are account activation / change password related or neccessary notifications according to your profile settings.
        </div>
      </div>  
      
    </div>);
}

