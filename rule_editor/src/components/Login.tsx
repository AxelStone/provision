import React, { useEffect } from "react";
import { CompProps, ErrorCodes } from "../consts";

export const Login: React.FC<CompProps> = (props: CompProps) => {

  useEffect(() => { 
    setTimeout(() => { 
      if ((window as any).gapi && !(window as any).gButtonRendered && 
        document.querySelector('#g-signin2') && !document.querySelector('#g-signin2 > div')
      ) { 
        (window as any).gButtonRendered = true;
        (window as any).gapi.signin2.render('g-signin2', {
          'scope': 'email', 'width': 262, 'height': 38, 'longtitle': true, 'theme': 'light',
          'onsuccess': (googleUser: any) => { document.dispatchEvent(new CustomEvent('google-signin', { detail: { googleUser }})) },
          'onfailure': (err: any) => { console.log('google err', err) }
        });
      }
    }, 100);
  })

  return (
    <div className="logincard">

      <h2>
        <div className="widthwrap"><i className="fas fa-user-circle"></i>LOG IN</div>
      </h2>

      { props.app.state.ui.mailsent ? <div className="mailsent message">
          <i className="far fa-envelope"></i> An account activation e-mail has been sent to your mailbox!
        </div> : ''
      }

      <div className="text">  
        <div className="widthwrap">     
          <p>Welcome to proVision Rule Editor v.0.2.0.</p>
        </div>
      </div>

      <div className="form">
        <div className="widthwrap">

          <div id="g-signin2"></div>

          <div className="divider">
            <span>OR</span>
          </div>

          <input id="email" placeholder="email"/>

          <input id="pwd" type="password" placeholder="password"/>

          { props.app.state.ui.error && props.app.state.ui.error.code == ErrorCodes.WRONG_USERNAME_PASSOWRD ? 
          <div className="error">Wrong username or password!</div> : null }

          { props.app.state.ui.error && props.app.state.ui.error.code == ErrorCodes.ACCOUNT_NOT_ACTIVATED ? 
          <div className="error">Account has not been activated yet!</div> : null }

          <div className="wrap">
            <div className="left">
              <button id="register-new" onClick={() => props.app.funcs.common.switchPage('register') }>
                Register new user
              </button><br />
              <button id="forgot-password" onClick={() => props.app.funcs.common.switchPage('recovery') }>
                Password recovery
              </button>
            </div>
            <div className="right">
              <button className="button" id="login" onClick={() => props.app.funcs.user.APILogin() }>
                { props.app.state.ui.loading !== 'login' && props.app.state.ui.loading !== 'validateuser' ? 
                  <i className="fas fa-key"></i> : <i className="fas fa-spinner fa-spin"></i> 
                }
                Login
              </button>
            </div>
          </div>
        </div>
      </div>

      <div className="text">  
        <div className="widthwrap">     
          <p>
            This is where we can define page-specific rules for the&nbsp;
            <a href="https://chrome.google.com/webstore/detail/provision/napdgmfnfbebjgahggnalabkkfaajldf" target="_blank">proVision Chrome extension</a>
            &nbsp;and hide any popup or unwanted element from any website, that is not hidden by automatic features of the extension. 
          </p> 
        </div> 
      </div>  


    </div>
  );
}

