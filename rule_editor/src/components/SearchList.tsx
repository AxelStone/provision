import React from "react";
import { CompProps } from "../consts";

export const SearchList: React.FC<CompProps> = (props: CompProps) => {

  return (
    <div className="searchlist">
      <div className="search">
        <input type="text" id="search-input" placeholder="Search.." 
          onKeyPress={(e) => e.code === 'Enter' ? props.app.funcs.common.search() : null }
        /><button onClick={(e) => props.app.funcs.common.search()}>
          <i className="fas fa-search"></i>
        </button>
      </div>

      <div className="items sc">
      { (props.app.state.ui.loading === 'search' || props.app.state.ui.loading === 'validateuser') ? 
          <div className="loading">
            <i className="fas fa-spinner fa-spin"></i>
          </div> : 
          props.app.state.rulesets.map(ruleset => ruleset.adult === '0' ?
            <div className="item" key={ruleset.name} onClick={ (e) => props.app.funcs.common.openRuleset(ruleset) }>
              { ruleset.name }
            </div> : '' 
          )
      }
           
      </div>  
    </div>
  );
}


