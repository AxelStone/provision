import React from "react";
import { CompProps } from "../consts";

export const UserBar: React.FC<CompProps> = (props: CompProps) => {

  return (
    <div className="wrap">
      <div className="left">
        { props.app.state.user ? <div>{props.app.state.user.user.nickname}</div> : <div>Anonymous</div> } 
      </div>
      <div className="right">
        { props.app.state.user ? <div>
          <button onClick={() => props.app.funcs.common.switchPage('profile')}>Profile</button> |&nbsp;
          <button onClick={() => props.app.funcs.user.APILogout()}>
            { props.app.state.ui.loading == 'logout' ? <i className="fas fa-spinner fa-spin"></i> : '' }
            Logout
          </button>
        </div> : <div>
          <button onClick={() => props.app.funcs.common.switchPage('register')}>Register</button> |&nbsp;
          <button onClick={() => props.app.funcs.common.switchPage('login')}>Login</button>
        </div>}
      </div>
    </div>
  );
}


