import React from "react";
import { useState } from "react";

class DeleteButton extends React.Component<{ id: string, for: string, onDelete: Function, loading: string | null }, { opened: boolean }> {

  state = { opened: false }

  listener = (ev: Event) => {
    let clikInside = false;
    let thisEl = document.getElementById('deletebutton-'+ this.props.for + '-delete-' + this.props.id) as HTMLElement; 
    if (thisEl) {
      Array.from(thisEl.querySelectorAll('*')).forEach(el => {
        if (el === ev.target) { clikInside = true; }
      })
      if (!clikInside) {
        this.setState((state) => ({ opened: false }))
      }
    }
  }

  setReallyOpened = (opened: boolean) => {
    this.setState((state) => ({ opened: opened }))
  }

  isLoading = () => {
    return (this.props.loading === this.props.for + '-delete-' + this.props.id)
  }
 
  componentDidMount = () => {
    document.addEventListener('click', this.listener)
  }

  componentWillUnmount = () => {
    document.removeEventListener('click', this.listener);
  }

  render() { 
    return (
      <div className="delete-button" id={'deletebutton-'+ this.props.for + '-delete-' + this.props.id}>
        
        <div className={"loading" + (this.isLoading() ? '' : ' hidden')}>
          <i className="fas fa-spinner fa-spin"></i>
        </div>  
           
        <button className={"delete" + (this.isLoading() || this.state.opened ? ' hidden' : '')} 
          onClick={() => { this.setReallyOpened(true) }}>
          <i className="fas fa-trash-alt"></i>
        </button> 
            
        <div className={"really" + (this.isLoading() || !this.state.opened ? ' hidden' : '') }> 
          <button className="yes" onClick={() => { this.props.onDelete(this.props.id) }}>
            <i className="fas fa-check"></i>
          </button>
          <button className="no" onClick={() => { this.setReallyOpened(false) }}>
            <i className="fas fa-times"></i>
          </button>
        </div> 

      </div>
    )
  }
}

export default DeleteButton;