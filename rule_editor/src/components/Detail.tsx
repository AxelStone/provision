import React from "react";
import { CompProps, FullRuleset } from "../consts";
import { EditRuleset } from "./EditRuleset";
import { Login } from "./Login";
import { Profile } from "./Profile";
import { Recovery } from "./Recovery";
import { Register } from "./Register";
import { Ruleset } from "./Ruleset";

export const Detail: React.FC<CompProps> = (props: CompProps) => {

  return (
    <div className="detail">
      
      <div className="controls">
        { props.app.state.ui.page === 'open-ruleset' && (props.app.state.detail.ruleset as FullRuleset).modified !== undefined ? 
          <button className={"button " + (!props.app.state.user ? 'disabled' : '')} id="suggest"
            onClick={() => props.app.state.user ? props.app.funcs.common.openSuggestUpdate() : null }
          >
            <i className="fas fa-edit"></i>Suggest update
          </button>
        : '' }
        <button className={"button " + (!props.app.state.user ? 'disabled' : '')} id="create"
          onClick={() => props.app.funcs.user.hasRights('create-post') ? props.app.funcs.common.switchPage('create-post') : null }>
          <i className="fas fa-folder-plus"></i>New ruleset
        </button>
      </div>

      <div className="page sc">
        { props.app.state.ui.page === 'login' ?         <Login app={props.app} /> : null } 
        { props.app.state.ui.page === 'register' ?      <Register app={props.app} /> : null } 
        { props.app.state.ui.page === 'recovery' ?      <Recovery app={props.app} /> : null } 
        { props.app.state.ui.page === 'profile' ?       <Profile app={props.app} /> : null }   
        { props.app.state.ui.page === 'open-ruleset' ?  <Ruleset app={props.app} /> : null }   
        { props.app.state.ui.page === 'create-post' ?   <EditRuleset app={props.app} /> : null }   
        { props.app.state.ui.page === 'edit-post' ?     <EditRuleset app={props.app} /> : null }
        { props.app.state.ui.page === 'create-update' ? <EditRuleset app={props.app} /> : null }
      </div>  

      <div className="links">
        <a href="https://www.axelproductions86.com/privacy-policy">Privacy Policy</a>
      </div> 
    </div>
  );
}


