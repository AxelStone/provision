import React from "react";
import { CompProps } from "../consts";

export const Header: React.FC<CompProps> = (props: CompProps) => {

  return (
    <header>
      <div>

        <a className="editorlogo" href="https://www.axelproductions86.com/provision-rule-editor" role="logo" title="proVision rule editor">
          <h1>
            <span className="icon" role="icon">&#123;<span>..</span>&#125;</span> 
            <div className="text">
              <div className="title"><strong>proVision</strong> <span>Rule Editor</span></div>
              <div className="descr">Create, edit, suggest, vote..</div>
            </div>
          </h1>
        </a>
      
        <a href="https://www.axelproductions86.com" title="Axel Productions 86 - Homepage" role="logo">
          <h2 className="logo small">
            AXEL
          </h2>
        </a>

      </div>
    </header>
  );
}


