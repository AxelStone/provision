import React from "react";
import { CompProps, ErrorCodes } from "../consts";

export const EditRuleset: React.FC<CompProps> = (props: CompProps) => {

  return (
    <div className="rulesetcard edit">
      
      { props.app.state.ui.page === 'create-post' ? <h2><i className="fas fa-folder-plus"></i>Create new ruleset</h2> :
        props.app.state.ui.page === 'edit-post'   ? <h2><i className="fas fa-edit"></i>Edit ruleset</h2> : 
        props.app.state.ui.page === 'create-update' ? <h2><i className="fas fa-edit"></i>Suggest update</h2> : ''
      }

      <div className="form">
        <div className="wrap">

          <div className="field name">
            <span className="label">Name (URL):</span>
            <input id="ruleset-name" placeholder="[sub.]domain[.tld]"
              onBlur={(e) => { props.app.funcs.main.validate('name', true) }}
              onChange={(e) => { props.app.funcs.main.setValid(false) }}
            />
            { props.app.state.ui.editor.nameExists ? 
            <div className="error">
              This ruleset name already exists! Please consider updating the existing ruleset if you see a room for improvement.
            </div> : null }
            { props.app.state.ui.editor.nameErr ? 
            <div className="error">This ruleset name is not valid!</div> : null }
          </div>

          <div className="field existing">
            <span className="label">Simmilar names:</span>
            <div className="value">{ props.app.state.detail.posts.map((s) => 
              <a>{s.name}</a>
            )}
            { props.app.state.detail.posts.length == 0 ? <span>N/A</span> : ''}
            </div>
          </div>

        </div>

        <div className="field ruleset">
          <span className="label">Ruleset (JSON):</span>
          <textarea className="value sc" id="ruleset-json" 
            defaultValue={JSON.stringify(JSON.parse('[{ "type": "", "selector": "" }]'), null, 2)}
            onBlur={(e) => { props.app.funcs.main.validate('json') }}
            onChange={(e) => { props.app.funcs.main.setValid(false) }}
          ></textarea>
          { props.app.state.ui.editor.jsonErr ? 
          <div className="error">{ props.app.state.ui.editor.jsonErr }</div> : null }
        </div>

        <div className="field mature">
          <label>
            <input type="checkbox" id="ruleset-mature-no" defaultChecked={true} onClick={(e) => props.app.funcs.main.setMature(false) }/>
            <span>Safe for all audiences</span>
          </label>
          <label>
            <input type="checkbox" id="ruleset-mature-yes" onClick={(e) => props.app.funcs.main.setMature(true) }/>
            <span>Mature content</span>
          </label>
        </div>

        { props.app.state.user?.user.roles !== '0' ?
          <div className="field documentation">
            <label>
              <input type="checkbox" id="ruleset-documentation" onChange={(e) => { props.app.funcs.main.validate('doc') }}/>
              <span>
                I have read and understood the <a href="https://www.axelproductions86.com/provision-documentation">documentation</a> 
                &nbsp;on how to create and update page-specific rulesets.
              </span>
            </label>
            { props.app.state.ui.editor.docErr ? 
              <div className="error">Please read the documentation first!</div> : null 
            }  
          </div> : '' }

        <div className="actions">

          <button id="ruleset-validate" 
            onClick={() => { 
              if(!props.app.state.ui.validated && !(props.app.state.ui.loading === 'ruleset-name')) { 
                props.app.funcs.main.validateAll(true) 
              }
            }}
            className={"button-validate " + 
              (props.app.state.ui.loading === 'ruleset-name' ? 'validating' : 
              !props.app.state.ui.validated ? '' : 
              (props.app.state.ui.editor.nameErr === null && 
              props.app.state.ui.editor.jsonErr === null && 
              props.app.state.ui.editor.docErr === null && 
              !props.app.state.ui.editor.nameExists) ? 'valid' : 'invalid')
            }>
            { props.app.state.ui.loading === 'ruleset-name' ? <i className="fas fa-spinner fa-spin"></i> : 
              !props.app.state.ui.validated ? <i className="fas fa-check-square"></i> :
              (props.app.state.ui.editor.nameErr === null && 
              props.app.state.ui.editor.jsonErr === null && 
              props.app.state.ui.editor.docErr === null && 
              !props.app.state.ui.editor.nameExists) ? <i className="fas fa-check"></i> : 
              <i className="fas fa-times"></i> 
            }          
            { props.app.state.ui.loading === 'ruleset-name' ? 'validating' : 
              !props.app.state.ui.validated ? 'validate' : 
              (props.app.state.ui.editor.nameErr === null && 
              props.app.state.ui.editor.jsonErr === null && 
              props.app.state.ui.editor.docErr === null && 
              !props.app.state.ui.editor.nameExists) ? 'valid': 
              'invalid'
            }
          </button>

          <button id="ruleset-send" 
            className={
              "button " + 
              (!props.app.state.ui.validated || 
              props.app.state.ui.loading !== null || 
              (props.app.state.ui.editor.nameErr !== null || 
              props.app.state.ui.editor.jsonErr !== null || 
              props.app.state.ui.editor.docErr !== null || 
              props.app.state.ui.editor.nameExists) ? 'disabled' : '')
            }
            disabled={
              !props.app.state.ui.validated || 
              props.app.state.ui.loading !== null || 
              (props.app.state.ui.editor.nameErr !== null || 
              props.app.state.ui.editor.jsonErr !== null || 
              props.app.state.ui.editor.docErr !== null || 
              props.app.state.ui.editor.nameExists)
            }
            onClick={() => { props.app.funcs.main.APISendRuleset() }}
          >
            { props.app.state.ui.loading === 'ruleset-send' ? 
              <span><i className="fas fa-spinner fa-spin"></i>Sending..</span>  :
              <span><i className="fas fa-paper-plane"></i>Send ruleset</span>
            } 
          </button>
        
        </div>
      </div>
      { props.app.state.user?.user.roles === "0" && props.app.state.ui.page === 'edit-post' ?
        <div className="admin-actions">
          <button id="ruleset-reject" className={"button " +
            (props.app.state.ui.loading !== null ? 'disabled' : '')}
            disabled={props.app.state.ui.loading !== null}
            onClick={() => { props.app.funcs.main.APIResolveRuleset(false) }}
          >
            <i className="fas fa-times"></i>Reject
          </button>
          <button id="ruleset-approve" className={"button " +           
            (!props.app.state.ui.validated || 
             props.app.state.ui.loading !== null || 
             props.app.state.ui.editor.nameErr !== null || 
             props.app.state.ui.editor.jsonErr !== null || 
             props.app.state.ui.editor.docErr !== null || 
             props.app.state.ui.editor.nameExists ? 'disabled' : '')
          } disabled={
            !props.app.state.ui.validated || 
            props.app.state.ui.loading !== null || 
            props.app.state.ui.editor.nameErr !== null || 
            props.app.state.ui.editor.jsonErr !== null || 
            props.app.state.ui.editor.docErr !== null || 
            props.app.state.ui.editor.nameExists
          } onClick={() => { props.app.funcs.main.APIResolveRuleset(true) }}
          >
            <i className="fas fa-check"></i>Approve
          </button>
        </div> : '' }

    </div>
  );
}


