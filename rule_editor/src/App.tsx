import React from 'react';
import './styles.scss';
import { DefaultState } from './consts';
import { getController } from './controllers/controller';
import { Header } from './components/Header';
import { Footer } from './components/Footer';
import { Detail } from './components/Detail';
import { SearchList } from './components/SearchList';
import { UserBar } from './components/UserBar';

class App extends React.Component {

  state = DefaultState;
  funcs = getController(this);

  componentDidMount = () => {
    (window as any).gButtonRendered = false

    // called after google signin button success
    document.addEventListener('google-signin', (res: any) => {
      if (!this.state.user && !this.funcs.user.getCookieToken()) {
        this.funcs.user.APIGoogleLogin(res.detail.googleUser)
      }
    })

    const queryParams = new URLSearchParams(window.location.search)
    const page = queryParams.get('page')
    console.log(page)
    switch (page) {
      case 'register': this.funcs.common.switchPage('register'); break
      case 'recovery': this.funcs.common.switchPage('recovery'); break
      default: this.funcs.user.APIValidateUser()
    } 

    this.funcs.common.search() 

  };

  render() {
    return (
      <div className="app">
        <Header app={this} />
        <div className="tab">
          <div className="user-bar">
            <UserBar app={this} />
          </div>
          <div className="content">
            <div className="panel-left">
              <SearchList app={this} />
            </div>
            <div className="panel-right">
              <Detail app={this} />
            </div>
          </div>
        </div>
        <Footer app={this} />
      </div>
    );
  }
}

export default App;
