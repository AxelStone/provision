import { ErrorCodes, RuleTypes } from "./consts";

export const Helper = {

  validateRulesetJson(json: string) {
    let error = null;
    if (json) {
      if (!Array.isArray(json) || json.length < 1) {
        error = 'JSON must be an array containing least 1 rule object!'
      } else {
        for(let i=json.length-1; i >= 0; i--) {
          // Mandatory fields
          if (typeof json[i].type === 'undefined') {
            error = 'Mandatory field "type" is missing! (rule index: '+i+')'
          } else if (typeof json[i].type !== 'string' || RuleTypes.indexOf(json[i].type) === -1) {
            error = 'Field "type" must contain one of these: "cookies", "push", "subscribe", "adblocker", "other"!'
          } else if (typeof json[i].selector === 'undefined') {
            error = 'Mandatory field "selector" is missing! (rule index: '+i+')'
          } else if (typeof json[i].selector !== 'string' || json[i].selector.length === 0) {
            error = 'Invalid value of "selector" field! (rule index: '+i+')'
          
          // Optional fields
          } else {
            if (typeof json[i].mustContain !== 'undefined') {
              if (typeof json[i].mustContain !== 'string' || json[i].mustContain.length == 0) {
                error = 'Invalid value of "mustContain" field! (rule index: '+i+')'
              }
            } if (typeof json[i].setStyle !== 'undefined') {
              if (Array.isArray(json[i].setStyle) && json[i].setStyle.length > 0) {
                for(let j=json[i].setStyle.length-1; j >= 0; j--) { console.log(typeof json[i].setStyle[j]);
                  if (typeof json[i].setStyle[j] !== 'object') {
                    error = 'An item of "setStyle" field is not an object (rule index: '+i+')!'
                  } else if (json[i].setStyle[j].for === undefined || json[i].setStyle[j].style === undefined){
                    error = 'An item of "setStyle" field must contain "for" and "style" fields (rule index: '+i+')!'
                  } else if (typeof json[i].setStyle[j].for !== 'string' || json[i].setStyle[j].for.length < 1) {
                    error = 'Invalid value of "setStyle: for" field (rule index: '+i+')!'
                  } else if (typeof json[i].setStyle[j].style !== 'string' || json[i].setStyle[j].style.length < 3 || json[i].setStyle[j].style.indexOf(':') === -1) {
                    error = 'Invalid value of "setStyle: style" field (rule index: '+i+')!'
                  }
                  // Unknown fields
                  for (let field in json[i].setStyle[j]) {
                    if (['for', 'style'].indexOf(field) === -1) {
                      error = 'Field "' + field + '" is not allowed! (rule index: '+i+')'
                    }
                  }
                }
              } else {
                error = 'Field "setStyle" must be an array containing at least 1 object (rule index: '+i+')!';
              }
            }
          }
          // Unknown fields
          for (let field in json[i]) {
            if (['type', 'selector', 'mustContain', "setStyle"].indexOf(field) === -1) {
              error = 'Field "' + field + '" is not allowed! (rule index: '+i+')'
            }
          }
        }
      }  

    } else {
      error = 'Invalid JSON syntax!'
    }
    return error;
  }
}